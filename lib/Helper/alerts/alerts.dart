import 'package:flutter/material.dart';
import 'package:flutter_easyloading/flutter_easyloading.dart';
 import 'package:get/get.dart';

class Alert {
  Alert._();


  static void showdialog(String msg)async
  {
    AbsorbPointer(absorbing: false,);
    await EasyLoading.show(
      status: msg,
      maskType: EasyLoadingMaskType.black,
    );
  }
  static void showsucces(String msg)async
  {
    AbsorbPointer(absorbing: true,);
    await
    EasyLoading.showSuccess(msg,duration: Duration(minutes: 1),dismissOnTap: true);

  }
  static void showerror(String msg)async
  {
    AbsorbPointer(absorbing: true,);
    await
    EasyLoading.showError(msg,duration: Duration(minutes: 1),dismissOnTap: true);

  }
  static void dismiss()async
  {
    AbsorbPointer(absorbing: true,);
    EasyLoading.dismiss();

  }

}