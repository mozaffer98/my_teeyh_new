import 'package:firebase_core/firebase_core.dart';
import 'package:flutter/material.dart';
import 'package:flutter_easyloading/flutter_easyloading.dart';
import 'package:flutter_localizations/flutter_localizations.dart';
import 'package:get/get.dart';
import 'package:my_teeyh/screen/Home/patientHome.dart';
import 'package:my_teeyh/screen/Profile/patientprofile.dart';
import 'package:my_teeyh/screen/auth/Register/PatientRegister/patient_register_home.dart';
import 'package:my_teeyh/screen/doctor_page.dart';
import 'package:my_teeyh/screen/home_page.dart';
import 'package:my_teeyh/screen/login_doctor.dart';
import 'package:my_teeyh/screen/login_page.dart';
import 'package:my_teeyh/screen/auth/Register/PatientRegister/login_patient.dart';
import 'package:my_teeyh/screen/patient_page.dart';
import 'package:my_teeyh/screen/register_page.dart';
import 'package:my_teeyh/screen/splash_screen.dart';
import 'package:my_teeyh/screen/varify_email.dart';
import 'package:my_teeyh/screen/splash_screen.dart';
import 'firebase_options.dart';

void main() async {
  WidgetsFlutterBinding.ensureInitialized();
  await Firebase.initializeApp(
    options: DefaultFirebaseOptions.currentPlatform,
  );
  runApp( MyteethApp());
}

class MyteethApp extends StatelessWidget {
   MyteethApp();

  @override
  Widget build(BuildContext context) {
    return GetMaterialApp(
      localizationsDelegates: [
        GlobalMaterialLocalizations.delegate,
        GlobalWidgetsLocalizations.delegate,
        GlobalCupertinoLocalizations.delegate,
      ],
      supportedLocales: [
        Locale('ar', ''), // English, no country code
      ],
      locale:Locale('ar') ,
      debugShowCheckedModeBanner: false,
      title: 'تطبيق أسناني',
      home: SplashScreen(),
      builder: EasyLoading.init(),
     // initialRoute: HomePage.id,
      routes: {
        HomePage.id: (context) => HomePage(),
        RegisterPage.id: (context) => RegisterPage(),
        LoginPatient.id: (context) => LoginPatient(),
        LoginDoctor.id: (context) => LoginDoctor(),
        LoginPage.id: (context) => LoginPage(),
        VarifyEmailPage.id: (context) => VarifyEmailPage(),
        DoctorPage.id: (context) => DoctorPage(),
        PatientPage.id: (context) => PatientPage()
      },
    );
  }
}
