import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:get/get.dart';
import 'package:my_teeyh/Helper/alerts/alerts.dart';
import 'package:my_teeyh/widgets/defualt_btn.dart';
import 'package:my_teeyh/widgets/defualt_text_form_field.dart';

class AddOffer extends StatefulWidget {
 final loginmethod, phone, email;
  const AddOffer({Key? key, this.loginmethod, this.phone, this.email}) : super(key: key);

  @override
  _AddOfferState createState() => _AddOfferState();
}

class _AddOfferState extends State<AddOffer> {
  TextEditingController _Offer_ctrl=TextEditingController();

  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    print(widget.loginmethod);
    print(widget.phone);
    print(widget.email);
  }
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text("إضافة عرض"),
      ),

      body: SingleChildScrollView(
        child: Column(
          children: [
            SizedBox(height: 20,),
            Center(
              child: CircleAvatar(
                radius: 90,
                backgroundColor: Color(0XFF3da7ff),
                child: CircleAvatar(
                  radius: 80,
                  backgroundImage: AssetImage('images/dent1.jpeg'),
                ),
              ),
            ),
            DefaultTextFormField1(
              type: TextInputType.text,
                hint: "تفاصيل العرض", controller: _Offer_ctrl),
            SizedBox(height: 20,),
            DefaultButton(text: "إضافة", onPress: (){
              AddOffer();
            })
          ],
        ),
      ),
    );
  }
  CollectionReference offerrRef =
  FirebaseFirestore.instance.collection("Offers");
  void AddOffer()async
  {
    Alert.showdialog("جار الاضافة");
    if(widget.loginmethod == "phone")
    {

      await offerrRef.add({
        "DoctorPhone":widget.phone,
        "OfferDetails":_Offer_ctrl.text
      });
      Alert.showsucces("تم إضافة العرض");
      Get.back();

    }
    else
    {
      await offerrRef.add({
        "DoctorPhone":widget.email,
        "OfferDetails":_Offer_ctrl.text
      });
      Alert.showsucces("تم إضافة العرض");
      Get.back();
    }
  }
}
