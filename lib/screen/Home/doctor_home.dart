import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:my_teeyh/screen/Home/add_offer.dart';
import 'package:my_teeyh/screen/Profile/doctor_profile.dart';
import 'package:my_teeyh/screen/home_page.dart';
import 'package:my_teeyh/screen/splash_screen.dart';
import 'package:my_teeyh/widgets/defualt_btn.dart';
import 'package:my_teeyh/widgets/drawer.dart';
import 'package:shared_preferences/shared_preferences.dart';

class DoctorHome extends StatefulWidget {
  final loginmethod, phone, email;
  const DoctorHome({Key? key, this.loginmethod, this.phone, this.email})
      : super(key: key);

  @override
  _DoctorHomeState createState() => _DoctorHomeState();
}

class _DoctorHomeState extends State<DoctorHome> {

  bool isLoading=false;
  String doctorname="";

  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    print(widget.loginmethod);
    print(widget.phone);
    print(widget.email);
    getDoctorName();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        automaticallyImplyLeading: false,
        title: Text("الرئيسية"),),
      endDrawer:isLoading? MainDrawerDoctor(
          onPressprofile: ()=> Get.to(DoctorProfile(loginmethod: widget.loginmethod, phone: widget.phone, email: widget.email,)),
          onpresslogout: ()=>logout(),
          onpressaddoffer: ()=> Get.to(AddOffer(loginmethod: widget.loginmethod, phone: widget.phone, email: widget.email,))):null,
      body:isLoading? SingleChildScrollView(
        child: Column(
          children: [
            Padding(

              padding: const EdgeInsets.all(18.0),
              child: Center(
                child: Text(
                  ' مرحبا بك دكتور  $doctorname  \n تطبيق أسناني نتمنى لك دوام النجاح والتوفيق',
                  textDirection: TextDirection.rtl,
                  style: TextStyle(
                    fontSize: 20,
                    fontWeight: FontWeight.bold,
                  ),
                ),
              ),
            ),
          ],
        ) ,
      ): Center(child: CircularProgressIndicator()),
      
    );
  }

  logout()async
  {
    Get.defaultDialog(
        title: 'تسجيل خروج',
        content: Text("هل تريد تسجيل الخروج؟"),
        onCancel: () => Get.back(),
        textCancel: 'لا',
        onConfirm: () async {
          SharedPreferences prefrence =
          await SharedPreferences.getInstance();
          prefrence.remove("loginmethod");
          prefrence.remove("loginmethodandvalue");
          Get.offAll(SplashScreen());
        },
        textConfirm: 'نعم',
        confirmTextColor: Colors.white);
  }

  getDoctorName()async
  {
    if(widget.loginmethod == "phone")
    {
      //FullName
      CollectionReference patientreference=FirebaseFirestore.instance.collection("Doctors");
      await patientreference.where("PhoneNumber",isEqualTo: "${widget.phone}").get().then((value)  async {
        setState(()   {
          doctorname=value.docs.elementAt(0)['FullName'].toString() ;
          isLoading = true;
        });
      });
    }
    else
    {
      CollectionReference patientreference=FirebaseFirestore.instance.collection("Doctors");
      await patientreference.where("Email",isEqualTo: "${widget.email.toString().trim()}").get().then((value)  async {
        setState(()   {
          doctorname=value.docs.elementAt(0)['FullName'].toString() ;
          isLoading = true;
        });
      });
    }
  }
}
