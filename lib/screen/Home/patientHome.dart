
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:my_teeyh/screen/Profile/patientprofile.dart';
import 'package:my_teeyh/screen/home_page.dart';
import 'package:my_teeyh/screen/splash_screen.dart';
import 'package:my_teeyh/widgets/drawer.dart';
import 'package:shared_preferences/shared_preferences.dart';

class PAtientHome extends StatefulWidget {
  final loginmethod,phone,email;
  const PAtientHome({Key? key, this.loginmethod, this.phone, this.email}) : super(key: key);

  @override
  _PAtientHomeState createState() => _PAtientHomeState();
}

class _PAtientHomeState extends State<PAtientHome> {

  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    print(widget.phone);
    print(widget.email);
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        automaticallyImplyLeading: false,
        title: Text("الرئيسية"),
        actions:[
          // IconButton(
          //     onPressed: (){Get.to(PatientProfile(loginmethod: widget.loginmethod,phone: widget.phone,email: widget.email,));},
          //   icon: Icon(Icons.person)),
          // IconButton(
          //     onPressed: (){
          //       Get.defaultDialog(
          //         title: 'تسجيل خروج',
          //         content: Text("هل تريد تسجيل الخروج؟"),
          //         onCancel:()=> Get.back() ,
          //         textCancel: 'لا',
          //         onConfirm: ()async{
          //           SharedPreferences prefrence=await SharedPreferences.getInstance();
          //           prefrence.remove("loginmethod");
          //           prefrence.remove("loginmethodandvalue");
          //           Get.offAll(SplashScreen());
          //         },
          //         textConfirm: 'نعم',
          //         confirmTextColor: Colors.white
          //       );
          //
          //     },
          //     icon: Icon(Icons.logout)),
        ],
      ),
      endDrawer: MainDrawer(
        onpresslogout:()=>logout() ,
        onPressprofile:()=>Get.to(PatientProfile(loginmethod: widget.loginmethod,phone: widget.phone,email: widget.email,)),

      ),
      body: SingleChildScrollView(
        child: Column(
          children: [
            Padding(
              padding: const EdgeInsets.all(18.0),
              child: Center(
              child: Text(
                'مرحبا بك عزيزي المريض  نتمنى لك الصحة والسلامة والف عافية.',
                textDirection: TextDirection.rtl,
                style: TextStyle(
                  fontSize: 20,
                  fontWeight: FontWeight.bold,
                ),
              ),
          ),
            ),

          ],
        ),
      ),
    );
  }
  logout()async
  {
        Get.defaultDialog(
                title: 'تسجيل خروج',
                content: Text("هل تريد تسجيل الخروج؟"),
                onCancel:()=> Get.back() ,
                textCancel: 'لا',
                onConfirm: ()async{
                  SharedPreferences prefrence=await SharedPreferences.getInstance();
                  prefrence.remove("loginmethod");
                  prefrence.remove("loginmethodandvalue");
                  Get.offAll(SplashScreen());
                },
                textConfirm: 'نعم',
                confirmTextColor: Colors.white
              );
  }
}

