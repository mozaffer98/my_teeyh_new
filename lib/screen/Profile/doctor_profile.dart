import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:country_code_picker/country_code_picker.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter/material.dart';
import 'package:flutter_easyloading/flutter_easyloading.dart';
import 'package:get/get.dart';
import 'package:my_teeyh/screen/otp.dart';

import '../../Helper/alerts/alerts.dart';

class DoctorProfile extends StatefulWidget {
  final loginmethod,phone,email;

  const DoctorProfile({Key? key, this.loginmethod, this.phone, this.email}) : super(key: key);

  @override
  _DoctorProfileState createState() => _DoctorProfileState();
}

class _DoctorProfileState extends State<DoctorProfile> {

  String CountryCode='+964', CountryCodesecond='+964';
  TextEditingController UserPassWord_Ctrl=TextEditingController();
  TextEditingController _Email_Ctrl=TextEditingController();
  TextEditingController name_Ctrl=TextEditingController();
  TextEditingController birthday_Ctrl=TextEditingController();
  TextEditingController diseasesl_Ctrl=TextEditingController();
  TextEditingController governoratel_Ctrl=TextEditingController();
  TextEditingController university_Ctrl=TextEditingController();
  TextEditingController FinalStage_Ctrl=TextEditingController();
  TextEditingController phone_Ctrl=TextEditingController();
  TextEditingController secondPhone_Ctrl=TextEditingController();
  TextEditingController cases_Ctrl=TextEditingController();
  TextEditingController suggest_Ctrl=TextEditingController();
  final _firestore=FirebaseFirestore.instance;
  bool isLoading=false,isLoading2=false;
  GlobalKey<FormState> formKey = GlobalKey();
  String? collectionId;
  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    print(widget.loginmethod);
    print(widget.phone);
    print(widget.email);
   getDoctorProfile();
    // getImageUrl("");
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: AppBar(title: Text("الملف الشخصي"), centerTitle: true,),
        body:
    isLoading == false  ?Center(child: CircularProgressIndicator()):
        Container(
          color: Colors.white,
          child:ListView(
            children: [
              Form(
                key: formKey  ,
                child: Padding(
                  padding: const EdgeInsets.all(20),
                  child: Column(
                    children: [
                      Container(
                        height: 45,
                      ),
                      Center(
                        child: CircleAvatar(
                          radius: 90,
                          backgroundColor: Color(0XFF3da7ff),
                          child: CircleAvatar(
                            radius: 80,
                            backgroundImage: AssetImage('images/dent1.jpeg'),
                          ),
                        ),
                      ),
                      Padding(
                        padding: const EdgeInsets.all(60),
                        child: Text(
                          'ملاحظة : هذه الاستمارة تسجيل دكتور أختصاص أو تسجيل طلبة المراحل الاخيرة الذين يحتاجون مراجعين مرضى تحت أشراف أطباء من حملة الشهادات العليا تكون  '
                              'أسعار الخدمة 5 الاف دينار للدكتور الاختصاص لكل حالة وبأسعار رمزية الى طلبة المراحل الاخيرة الذين يحتاجون مراجعين مرضى',
                          textDirection: TextDirection.rtl ,
                          style: TextStyle(
                            fontWeight: FontWeight.w900,
                          ),
                          textAlign: TextAlign.justify,
                        ),
                      ),
                      Text(
                        'هذه الاسعار تكلفة أدامة وتطوير التطبيق',
                        textDirection: TextDirection.rtl ,
                        style: TextStyle(
                          fontWeight: FontWeight.w900,
                        ),
                      ),
                      Padding(
                        padding: const EdgeInsets.all(15),
                        child: Text(
                          'الاسعار',
                          textDirection: TextDirection.rtl ,
                          style: TextStyle(
                            fontWeight: FontWeight.w900,
                          ),
                        ),
                      ),
                      Text(
                        '  تنظيف الاسنان 10 الاف دينار عراقي ',
                        textDirection: TextDirection.rtl ,
                        style: TextStyle(fontWeight: FontWeight.w900),
                      ),
                      Text(
                        '  قلع الاسنان 15 الاف دينار عراقي',
                        textDirection: TextDirection.rtl ,
                        style: TextStyle(fontWeight: FontWeight.w900),
                      ),
                      Text(
                        '  حشوة الاسنان 15 الف دينار عراقي',
                        textDirection: TextDirection.rtl ,
                        style: TextStyle(fontWeight: FontWeight.w900),
                      ),
                      Text(
                        ' طقم الاسنان او تقويم الاسنان 25 الف دينار عراقي',
                        textDirection: TextDirection.rtl ,
                        style: TextStyle(fontWeight: FontWeight.w900),
                      ),
                      Padding(
                        padding: const EdgeInsets.all(8),
                        child: Text(
                          'الدفع عن طريق خدمة زين كاش أو أسيا حوالة وللأستفسار اكثر',
                          textDirection: TextDirection.rtl ,
                          style: TextStyle(
                            fontWeight: FontWeight.w900,
                          ),
                        ),
                      ),
                      Text(
                        '07703989069',
                        textDirection: TextDirection.rtl ,
                        style: TextStyle(fontWeight: FontWeight.w900),
                      ),
                      Text(
                        '07811555589',
                        textDirection: TextDirection.rtl ,
                        style: TextStyle(fontWeight: FontWeight.w900),
                      ),
                      SizedBox(
                        height: 15,
                      ),
                      TextFormField(
                        validator: (data) {
                          if (data!.isEmpty) {
                            return 'This Question Is Required - هذا السؤال مطلوب';
                          }
                        },
                        controller: name_Ctrl,
                        keyboardType: TextInputType.text,
                        decoration: InputDecoration(
                          labelText: ' الاسم الثلاثي Full Name ',
                          prefixIcon: Icon(Icons.person),
                        ),
                      ),
                      SizedBox(
                        height: 15,
                      ),
                      TextFormField(
                        controller: birthday_Ctrl,
                        validator: (data) {
                          if (data!.isEmpty ) {
                            return 'This Question Is Required - هذا السؤال مطلوب';
                          }
                        },
                        keyboardType: TextInputType.number,
                        decoration: InputDecoration(
                          labelText: 'العمر - The Age',
                          prefixIcon: Icon(Icons.edit_calendar_rounded),
                        ),
                      ),
                      SizedBox(
                        height: 15,
                      ),
                      TextFormField(
                        controller: FinalStage_Ctrl,
                        validator: (data) {
                          if (data!.isEmpty) {
                            return 'This Question Is Required - هذا السؤال مطلوب';
                          }
                        },
                        keyboardType: TextInputType.text,
                        decoration: InputDecoration(
                          labelText:
                          'دكتور أختصاص أو طالب مرحلة اخيرة  Specialist Doctor Or Final Stage Student',
                          prefixIcon: Icon(Icons.medical_information_rounded),
                        ),
                      ),
                      SizedBox(
                        height: 15,
                      ),
                      TextFormField(
                        controller: governoratel_Ctrl,
                        validator: (data) {
                          if (data!.isEmpty) {
                            return 'This Question Is Required - هذا السؤال مطلوب';
                          }
                        },
                        keyboardType: TextInputType.text,
                        decoration: InputDecoration(
                          labelText: 'المحافظة والمدينة - Governorate And The City',
                          prefixIcon: Icon(Icons.place_rounded),
                        ),
                      ),
                      SizedBox(
                        height: 15,
                      ),
                      TextFormField(
                        controller: university_Ctrl,
                        validator: (data) {
                          if (data!.isEmpty) {
                            return 'This Question Is Required - هذا السؤال مطلوب';
                          }
                        },
                        keyboardType: TextInputType.text,
                        decoration: InputDecoration(
                          labelText: 'الجامعة - The University',
                          prefixIcon: Icon(Icons.school_rounded),
                        ),
                      ),
                      SizedBox(
                        height: 15,
                      ),
                      widget.loginmethod=="phone"?  Column(
                        children: [
                          TextFormField(
                            validator: (data) {
                              if (data!.isEmpty && widget.loginmethod=="phone") {
                                return 'This Question Is Required - هذا السؤال مطلوب';
                              }
                            },
                            controller: phone_Ctrl,
                            keyboardType: TextInputType.phone,
                            decoration: InputDecoration(
                                labelText: 'رقم الهاتف الاول - First Phone Number',
                                prefixIcon:  CountryCodePicker(
                                  onChanged: (c) {
                                    CountryCode = c.dialCode!;
                                    print(CountryCode);setState(() {});
                                  },
                                  initialSelection:CountryCode,
                                  showCountryOnly: false,
                                  showOnlyCountryWhenClosed: false,
                                  alignLeft: false,
                                )
                            ),
                          ),
                          SizedBox(
                            height: 15,
                          ),
                          TextFormField(
                            controller: secondPhone_Ctrl,
                            validator: (data) {
                              if (data!.isEmpty && widget.loginmethod=="phone") {
                                return 'This Question Is Required - هذا السؤال مطلوب';
                              }
                            },
                            keyboardType: TextInputType.phone,
                            decoration: InputDecoration(
                                labelText: 'رقم الهاتف الثاني - Second Phone Number',
                                prefixIcon:  CountryCodePicker(
                                  onChanged: (c) {
                                    CountryCodesecond = c.dialCode!;
                                    print(CountryCodesecond);setState(() {});
                                  },
                                  initialSelection:CountryCode,
                                  showCountryOnly: false,
                                  showOnlyCountryWhenClosed: false,
                                  alignLeft: false,
                                )
                            ),
                          ),
                        ],
                      ): Column(
                        children: [
                          TextFormField(
                            enabled: false,
                            controller: _Email_Ctrl,
                            onChanged: (value) {},
                            validator: (data) {
                              if (data!.isEmpty && widget.loginmethod !="phone") {
                                return 'This Question Is Required - هذا السؤال مطلوب';
                              }
                            },
                            decoration: InputDecoration(hintText: 'ادخل البريد الالكتروني',
                            ),
                          ),
                          SizedBox(height: 8),

                          TextFormField(
                            enabled: false,
                            obscureText: true,
                            controller: UserPassWord_Ctrl,
                            onChanged: (value) {},
                            validator: (data) {
                              if (data!.isEmpty && widget.loginmethod !="phone") {
                                return 'This Question Is Required - هذا السؤال مطلوب';
                              }
                            },
                            decoration: InputDecoration(
                              hintText: 'ادخل كلمة المرور',

                            ),
                          ),
                        ],
                      ),
                      SizedBox(
                        height: 15,
                      ),
                      TextFormField(
                        controller: cases_Ctrl,
                        validator: (data) {
                          if (data!.isEmpty) {
                            return 'This Question Is Required - هذا السؤال مطلوب';
                          }
                        },
                        keyboardType: TextInputType.text,
                        decoration: InputDecoration(
                          labelText:
                          'مالحالات التي ترغب بها ( التنظيف - القلع - الحشوات - التقويم ) What cases do you want (cleaning - extractions - fillings - orthodontics)',
                          prefixIcon: Icon(Icons.question_mark_rounded),
                        ),
                      ),
                      SizedBox(
                        height: 15,
                      ),

                      TextFormField(
                        controller: suggest_Ctrl,
                        validator: (data) {
                          if (data!.isEmpty) {
                            return 'This Question Is Required - هذا السؤال مطلوب';
                          }
                        },
                        keyboardType: TextInputType.text,
                        decoration: InputDecoration(
                          labelText:
                          'ماذا تقترح لتطوير التطبيق - What do you suggest to develop the application',
                          prefixIcon: Icon(Icons.settings_suggest_outlined),
                        ),
                      ),
                      SizedBox(
                        height: 30,
                      ),
                      Container(
                        height: 40,
                        width: double.infinity,
                        color: Color(0XFF3da7ff),
                        child: MaterialButton(
                          onPressed: ()  {
                            var formdata = formKey.currentState;
                            if (formdata != null) {
                              if (formdata.validate()) {
                                formdata.save();
                               getDoctor();
                              }
                            }
                          },
                          child: Text(
                            'أرسال المعلومات - Send Information',
                            style: TextStyle(
                              fontWeight: FontWeight.w900,
                              color: Colors.white,
                            ),
                          ),
                        ),
                      ),
                    ],
                  ),
                ),
              ),
            ],
          ),
        )
    );
  }

  void getDoctorProfile()async
  {
    if(widget.loginmethod == "phone")
    {
      //FullName
      CollectionReference patientreference=FirebaseFirestore.instance.collection("Doctors");
      await patientreference.where("PhoneNumber",isEqualTo: "${widget.phone}").get().then((value)  async {

        setState(()   {
          collectionId= value.docs.elementAt(0).id;
          print("${collectionId} idddddddddddd");
          name_Ctrl.text=value.docs.elementAt(0)['FullName'].toString() ;
          phone_Ctrl.text=value.docs.elementAt(0)['PhoneNumber'].replaceAll('+964','').toString() ;
          birthday_Ctrl.text= value.docs.elementAt(0)['birthday'].toString();
          governoratel_Ctrl.text =value.docs.elementAt(0)['Governorate'].toString();
          FinalStage_Ctrl.text =value.docs.elementAt(0)['FinalStage'].toString() ;
          secondPhone_Ctrl.text =value.docs.elementAt(0)['SecondPhoneNumber'].replaceAll('+964','').toString() ;
          cases_Ctrl.text =value.docs.elementAt(0)['Cases'].toString() ;
          suggest_Ctrl.text =value.docs.elementAt(0)['suggestion'].toString();
          university_Ctrl.text =value.docs.elementAt(0)['Univesity'].toString();
          isLoading = true;
        });
      });
    }
    else
    {
      CollectionReference patientreference=FirebaseFirestore.instance.collection("Doctors");
      await patientreference.where("Email",isEqualTo: "${widget.email.toString().trim()}").get().then((value)  async {
        setState(()   {
          collectionId= value.docs.elementAt(0).id;
          print("${collectionId} idddddddddddd");
          name_Ctrl.text=value.docs.elementAt(0)['FullName'].toString() ;
          _Email_Ctrl.text=value.docs.elementAt(0)['Email'].toString() ;
          birthday_Ctrl.text= value.docs.elementAt(0)['birthday'].toString();
          governoratel_Ctrl.text =value.docs.elementAt(0)['Governorate'].toString();
          FinalStage_Ctrl.text =value.docs.elementAt(0)['FinalStage'].toString() ;
          UserPassWord_Ctrl.text =value.docs.elementAt(0)['Password'].toString();
          cases_Ctrl.text =value.docs.elementAt(0)['Cases'].toString() ;
          suggest_Ctrl.text =value.docs.elementAt(0)['suggestion'].toString();
          university_Ctrl.text =value.docs.elementAt(0)['Univesity'].toString();
          isLoading = true;
        });
      });
    }
  }
  updateInfo()async
  {
    if(widget.loginmethod =="phone")
    {
      CollectionReference   patientreference=FirebaseFirestore.instance.collection("Doctors");
      patientreference.doc(collectionId).update({
        "FullName":name_Ctrl.text,
        "PhoneNumber":"${CountryCode}${phone_Ctrl.text}",
        "birthday": birthday_Ctrl.text,
        "Governorate":governoratel_Ctrl.text ,
        "SecondPhoneNumber":"${CountryCodesecond}${secondPhone_Ctrl.text}",
        "FinalStage":FinalStage_Ctrl.text,
        "email":"",
        "password":"",
        "Cases":cases_Ctrl.text,
        "suggestion":suggest_Ctrl.text,
        "Univesity":university_Ctrl.text,
        "signInMethod":"phone",
      });
      Alert.showsucces("تم ارسال المعلومات سيتم التواصل معك بأقرب وقت");
      Get.back();
    }
    else
    {

      CollectionReference   patientreference=FirebaseFirestore.instance.collection("Doctors");
      patientreference.doc(collectionId).update({
        "FullName":name_Ctrl.text,
        "birthday": birthday_Ctrl.text,
        "Governorate":governoratel_Ctrl.text ,
        "FinalStage":FinalStage_Ctrl.text,
        "Cases":cases_Ctrl.text,
        "suggestion":suggest_Ctrl.text,
        "Univesity":university_Ctrl.text,
        "signInMethod":"email",
      });
      Alert.showsucces("تم ارسال المعلومات سيتم التواصل معك بأقرب وقت");
      Get.back();
    }
  }

  Future<void> getDoctor() async {
    if(widget.loginmethod == "phone")
    {
      if ("${widget.phone}" == "${CountryCode}${phone_Ctrl.text}") {
        print("saameeeeeeee***********");
        updateInfo();
      } else {
        print("not saameeeeeeee***********");
        Alert.showdialog("...");
        var collection = FirebaseFirestore.instance.collection('Patients');
        var querySnapshot = await collection
            .where('PhoneNumber', isEqualTo: "${CountryCode}${phone_Ctrl.text}")
            .get();
        if (querySnapshot.docs.length > 0) {
          Alert.showerror("يوجد مستخدم برقم الهاتف هذا");
        } else {
          EasyLoading.dismiss();
          varefynumberFirebase();
        }
      }
    }
    else
    {
      updateInfo();
    }
  }
  FirebaseAuth _auth = FirebaseAuth.instance;
  void varefynumberFirebase() async{
    Alert.showdialog("...");
    await _auth.verifyPhoneNumber(
      phoneNumber:"${CountryCode}${phone_Ctrl.text} ",
      verificationCompleted: (phoneAuthCredential) async {
        print('fffffffffffffffffffffffff');
        //   EasyLoading.dismiss();
        setState(() {
        });
        //signInWithPhoneAuthCredential(phoneAuthCredential);
      },
      verificationFailed: (verificationFailed) async {
        setState(() {
        });
        EasyLoading.showError( "الرقم غير صالح");
      },
      codeSent: (verificationId, resendingToken) async {
        setState(() {
          print("seeeeeeeeeee");
          EasyLoading.dismiss();
          Get.to(OtpScreen(isLogin: false,varifecationId:verificationId ,phone:"${CountryCode}${phone_Ctrl.text}",register: updateInfo),);
        });
      },
      codeAutoRetrievalTimeout: (verificationId) async {
      },
    );
  }
}
