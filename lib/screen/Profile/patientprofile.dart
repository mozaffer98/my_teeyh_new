import 'dart:io';
import 'dart:math';
import 'package:awesome_dialog/awesome_dialog.dart';
import 'package:flutter_easyloading/flutter_easyloading.dart';
import 'package:get/get.dart';
import 'package:my_teeyh/Helper/alerts/alerts.dart';
import 'package:my_teeyh/screen/otp.dart';
import 'package:my_teeyh/widgets/drawer.dart';
import 'package:path/path.dart';
import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:country_code_picker/country_code_picker.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:firebase_storage/firebase_storage.dart';
import 'package:flutter/material.dart';
import 'package:firebase_storage/firebase_storage.dart' as firbaseStorage;
import 'package:flutter/painting.dart';
import 'package:image_picker/image_picker.dart';


class PatientProfile extends StatefulWidget {
  final loginmethod,phone,email;
  const PatientProfile({Key? key, this.loginmethod, this.phone, this.email}) : super(key: key);

  @override
  _PatientProfileState createState() => _PatientProfileState();
}

class _PatientProfileState extends State<PatientProfile> {

  bool isLoading=false,isLoading2=false,enabled=false;
  String CountryCode='+964', CountryCodesecond='+964';
  TextEditingController UserPassWord_Ctrl=TextEditingController();
  TextEditingController _Email_Ctrl=TextEditingController();
  TextEditingController name_Ctrl=TextEditingController();
  TextEditingController birthday_Ctrl=TextEditingController();
  TextEditingController diseasesl_Ctrl=TextEditingController();
  TextEditingController governoratel_Ctrl=TextEditingController();
  TextEditingController university_Ctrl=TextEditingController();
  TextEditingController phone_Ctrl=TextEditingController();
  TextEditingController secondPhone_Ctrl=TextEditingController();
  TextEditingController cases_Ctrl=TextEditingController();
  TextEditingController suggest_Ctrl=TextEditingController();

  final _firestore=FirebaseFirestore.instance;

  GlobalKey<FormState> formKey = GlobalKey();
  File? file;
  String? name;

  String? birthday;

  String? diseases;

  String? governorate;

  String? university;

  String? phone;

  String? secondPhone;

  String? email;

  String? cases;

  String? suggest;
  String? imageurl="",collectionId="";

  Reference? ref;
 // String? name,age,phone,email,imagrUrl;
  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    print(widget.phone);
    print(widget.email);
    print(widget.loginmethod);
   getPatientProfile();
    // getImageUrl("");
  }
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(title: Text("الملف الشخصي"), centerTitle: true,),

      body:
    isLoading == false  ?Center(child: CircularProgressIndicator()):
      Container(
        color: Colors.white,
        child:ListView(
        children: [
          Form(
            key: formKey  ,
            child: Padding(
              padding: const EdgeInsets.all(20),
              child: Column(
                children: [
                  // Center(
                  //   child: Text(
                  //     'تسجيل مريض Patient Registration',
                  //     style: TextStyle(
                  //       fontSize: 20,
                  //       fontWeight: FontWeight.bold,
                  //     ),
                  //   ),
                  // ),
                  Container(
                    height: 45,
                  ),
                  Center(
                    child: CircleAvatar(
                      radius: 90,
                      backgroundColor: Color(0XFF3da7ff),
                      child: CircleAvatar(
                        radius: 80,
                        backgroundImage: AssetImage('images/dent1.jpeg'),
                      ),
                    ),
                  ),
                  Padding(
                    padding: const EdgeInsets.all(60),
                    child:
                    Text(
                      'ملاحظة : هذه الاستمارة تسجيل مريض يحتاج الى معالجة الاسنان حيث أن تطبيق أسناني يوفر هذه الخدمة مجانا للمرضى تحت أشراف أطباء من حملة الشهادات العليا  مراعاة للظروف العراق والظروف الانسانية وأيضا يوفر خدمة أختيار طبيبك '
                          'بأسعار مناسبة'
                          ' يرجى من المريض اضافة صورة حالة الاسنان لكي يتم تقييم الحالة من قبل الدكتور بصورة دقيقة',
                      textDirection: TextDirection.rtl ,
                      style: TextStyle(
                        fontWeight: FontWeight.w900,
                      ),
                      textAlign: TextAlign.justify,
                    ),
                  ),
                  Text(
                    'للمساعدة والاستفسار أكثر يرجى التواصل على الرقم التالي',
                    style: TextStyle(
                      fontWeight: FontWeight.w900,
                    ),
                    textAlign: TextAlign.center,
                  ),
                  Text(
                    ' 07703989069',
                    style: TextStyle(
                      fontWeight: FontWeight.w900,
                    ),
                  ),
                  Text(
                    ' 07811555589',
                    style: TextStyle(
                      fontWeight: FontWeight.w900,
                    ),
                  ),
                  TextFormField(
                    validator: (data) {
                      if (data!.isEmpty) {
                        return 'This Question Is Required - هذا السؤال مطلوب';
                      }
                    },
                    controller: name_Ctrl,
                    onSaved: ((value) => name = value!),
                    keyboardType: TextInputType.text,
                    decoration: InputDecoration(
                      labelText: ' الاسم الثلاثي Full Name ',
                      prefixIcon: Icon(Icons.person),
                    ),
                  ),
                  SizedBox(
                    height: 15,
                  ),
                    TextFormField(
                      controller: birthday_Ctrl,
                    onSaved: ((value) => birthday = value!),
                    validator: (data) {
                      if (data!.isEmpty ) {
                        return 'This Question Is Required - هذا السؤال مطلوب';
                      }
                    },
                    keyboardType: TextInputType.number,
                    decoration: InputDecoration(
                      labelText: 'العمر - The Age',
                      prefixIcon: Icon(Icons.edit_calendar_rounded),
                    ),
                  ),
                  SizedBox(
                    height: 15,
                  ),
                  TextFormField(
                    controller: governoratel_Ctrl,
                    onSaved: ((value) => governorate = value!),
                    validator: (data) {
                      if (data!.isEmpty) {
                        return 'This Question Is Required - هذا السؤال مطلوب';
                      }
                    },
                    keyboardType: TextInputType.text,
                    decoration: InputDecoration(
                      labelText: 'المحافظة والمدينة - Governorate And The City',
                      prefixIcon: Icon(Icons.place_rounded),
                    ),
                  ),
                  SizedBox(
                    height: 15,
                  ),
                  TextFormField(
                    controller: diseasesl_Ctrl,
                    onSaved: ((value) => diseases = value!),
                    validator: (data) {
                      if (data!.isEmpty) {
                        return 'This Question Is Required - هذا السؤال مطلوب';
                      }
                    },
                    keyboardType: TextInputType.text,
                    decoration: InputDecoration(
                      labelText:
                      'هل لديك أمراض مزمنة - Do you have chronic diseases',
                      prefixIcon: Icon(Icons.sick_rounded),
                    ),
                  ),
                  SizedBox(
                    height: 15,
                  ),
                widget.loginmethod=="phone"?  Column(
                    children: [
                      TextFormField(

                        validator: (data) {
                          if (data!.isEmpty && widget.loginmethod=="phone") {
                            return 'This Question Is Required - هذا السؤال مطلوب';
                          }
                        },
                        controller: phone_Ctrl,
                        onSaved: ((value) => phone = value!),
                        keyboardType: TextInputType.phone,
                        decoration: InputDecoration(
                            labelText: 'رقم الهاتف الاول - First Phone Number',
                            prefixIcon:  CountryCodePicker(
                              onChanged: (c) {
                                CountryCode = c.dialCode!;
                                print(CountryCode);setState(() {});
                              },
                              initialSelection:CountryCode,
                              showCountryOnly: false,
                              showOnlyCountryWhenClosed: false,
                              alignLeft: false,
                            )
                        ),
                      ),
                      SizedBox(
                        height: 15,
                      ),
                      TextFormField(
                        controller: secondPhone_Ctrl,
                        onSaved: ((value) => secondPhone = value!),
                        validator: (data) {
                          if (data!.isEmpty && widget.loginmethod=="phone") {
                            return 'This Question Is Required - هذا السؤال مطلوب';
                          }
                        },
                        keyboardType: TextInputType.phone,
                        decoration: InputDecoration(
                            labelText: 'رقم الهاتف الثاني - Second Phone Number',
                            prefixIcon:  CountryCodePicker(
                              onChanged: (c) {
                                CountryCodesecond = c.dialCode!;
                                print(CountryCodesecond);setState(() {});
                              },
                              initialSelection:CountryCode,
                              showCountryOnly: false,
                              showOnlyCountryWhenClosed: false,
                              alignLeft: false,
                            )
                        ),
                      ),
                    ],
                  ): Column(
                    children: [
                      TextFormField(
                        enabled: false,
                        controller: _Email_Ctrl,
                        onChanged: (value) {},
                        validator: (data) {
                          if (data!.isEmpty && widget.loginmethod !="phone") {
                            return 'This Question Is Required - هذا السؤال مطلوب';
                          }
                        },
                        decoration: InputDecoration(hintText: 'ادخل البريد الالكتروني',
                        ),
                      ),
                      SizedBox(height: 8),

                      TextFormField(
                        enabled: false,

                        obscureText: true,
                        controller: UserPassWord_Ctrl,
                        onChanged: (value) {},
                        validator: (data) {
                          if (data!.isEmpty && widget.loginmethod !="phone") {
                            return 'This Question Is Required - هذا السؤال مطلوب';
                          }
                        },
                        decoration: InputDecoration(
                          hintText: 'ادخل كلمة المرور',

                        ),
                      ),
                    ],
                  ),
                  SizedBox(
                    height: 15,
                  ),
                  TextFormField(
                    controller: cases_Ctrl,
                    onSaved: ((value) => cases = value!),
                    validator: (data) {
                      if (data!.isEmpty) {
                        return 'This Question Is Required - هذا السؤال مطلوب';
                      }
                    },
                    keyboardType: TextInputType.text,
                    decoration: InputDecoration(
                      labelText:
                      'مالحالات التي ترغب بها ( التنظيف - القلع - الحشوات - التقويم ) What cases do you want (cleaning - extractions - fillings - orthodontics)',
                      prefixIcon: Icon(Icons.question_mark_rounded),
                    ),
                  ),
                  SizedBox(
                    height: 15,
                  ),
                  Text(
                    'اختيار صورة حالة المريض - Choose Patient Case Picture',
                    textDirection: TextDirection.rtl,
                    style: TextStyle(
                      fontWeight: FontWeight.w500,
                    ),
                  ),
                  SizedBox(
                    height: 15,
                  ),
                  Stack(
                    children: [
                      CircleAvatar(
                        radius: 125,
                        backgroundColor: Color(0XFF3da7ff),
                        child: CircleAvatar(
                          radius: 120,
                          backgroundColor: Colors.white,
                          backgroundImage:
                          file == null ? null : FileImage(file!),
                        ),
                      ),
                      Positioned(
                        top: 180,
                        child: RawMaterialButton(
                          elevation: 10,
                          fillColor: Color(0XFF3da7ff),
                          child: Icon(Icons.add_a_photo),
                          padding: EdgeInsets.all(15),
                          shape: CircleBorder(),
                          onPressed: () {
                            showDialog(
                                context: context,
                                builder: (BuildContext context) {
                                  return AlertDialog(
                                    title: Text(
                                      'اختيار صورة - Choose Picture',
                                      style: TextStyle(
                                        fontWeight: FontWeight.w700,
                                      ),
                                    ),
                                    content: SingleChildScrollView(
                                      child: ListBody(
                                        children: [
                                          InkWell(
                                            onTap: () {
                                              _imageFromCamera();
                                              Navigator.pop(context);
                                            },
                                            splashColor: Color(0XFF3da7ff),
                                            child: Row(
                                              children: [
                                                Padding(
                                                  padding:
                                                  const EdgeInsets.all(8.0),
                                                  child: const Icon(
                                                      Icons.camera_alt_rounded),
                                                ),
                                                Text(
                                                  'Camera - الكاميرا',
                                                  style: TextStyle(
                                                    fontSize: 18,
                                                    fontWeight: FontWeight.w500,
                                                    color: Color(0XFF3da7ff),
                                                  ),
                                                ),
                                              ],
                                            ),
                                          ),
                                          InkWell(
                                            onTap: () {
                                              _imageFromGallery();
                                              Navigator.pop(context);
                                            },
                                            splashColor: Color(0XFF3da7ff),
                                            child: Row(
                                              children: [
                                                Padding(
                                                  padding:
                                                  const EdgeInsets.all(8.0),
                                                  child: const Icon(
                                                      Icons.image_rounded),
                                                ),
                                                Text(
                                                  'Gallery - الاستوديو',
                                                  style: TextStyle(
                                                    fontSize: 18,
                                                    fontWeight: FontWeight.w500,
                                                    color: Color(0XFF3da7ff),
                                                  ),
                                                ),
                                              ],
                                            ),
                                          ),
                                        ],
                                      ),
                                    ),
                                  );
                                });
                          },
                        ),
                      ),
                    ],
                  ),
                  TextFormField(
                    controller: suggest_Ctrl,
                    validator: (data) {
                      if (data!.isEmpty) {
                        return 'This Question Is Required - هذا السؤال مطلوب';
                      }
                    },
                    keyboardType: TextInputType.text,
                    decoration: InputDecoration(
                      labelText:
                      'ماذا تقترح لتطوير التطبيق - What do you suggest to develop the application',
                      prefixIcon: Icon(Icons.settings_suggest_outlined),
                    ),
                  ),
                  SizedBox(
                    height: 30,
                  ),
                  Container(
                    height: 40,
                    width: double.infinity,
                    color: Color(0XFF3da7ff),
                    child: MaterialButton(
                      onPressed: ()  {
                        var formdata = formKey.currentState;
                        if (formdata != null) {
                          if (formdata.validate()) {
                            formdata.save();
                            getPatient();
                          }
                        }
                      },
                      child: Text(
                        'أرسال المعلومات - Send Information',
                        style: TextStyle(
                          fontWeight: FontWeight.w900,
                          color: Colors.white,
                        ),
                      ),
                    ),
                  ),
                ],
              ),
            ),
          ),
        ],
      ),
      )
    );
  }

  void getPatientProfile()async
  {
    if(widget.loginmethod == "phone")
      {
        //FullName
        CollectionReference patientreference=FirebaseFirestore.instance.collection("Patients");
        await patientreference.where("phoneNumber",isEqualTo: "${widget.phone}").get().then((value)  async {
         print(value.docs.elementAt(0)['full_Name'].toString());
          setState(()   {

            collectionId= value.docs.elementAt(0).id;

            print("${collectionId} idddddddddddd");
            // _UserPassWord_Ctrl
            // _Email_Ctrl=TextEd
            name_Ctrl.text=value.docs.elementAt(0)['full_Name'].toString() ;
            birthday_Ctrl.text= value.docs.elementAt(0)['birthday'].toString();
            diseasesl_Ctrl.text = value.docs.elementAt(0)['diseases'].toString();
            governoratel_Ctrl.text =value.docs.elementAt(0)['governorate'].toString();
            phone_Ctrl.text =value.docs.elementAt(0)['phoneNumber'].replaceAll('+964','').toString()  ;
            secondPhone_Ctrl.text =value.docs.elementAt(0)['secondPhoneNumber'].replaceAll('+964','').toString() ;
            cases_Ctrl.text =value.docs.elementAt(0)['cases'].toString() ;
            suggest_Ctrl.text =value.docs.elementAt(0)['suggestion'].toString();
            isLoading = true;
          });
        });
      }
    else
      {
        CollectionReference patientreference=FirebaseFirestore.instance.collection("Patients");
        await patientreference.where("email",isEqualTo: widget.email.toString().trim()).get().then((value) async{
        //  print(value.docs.elementAt(0)['full_Name'].toString());
          setState(() {
           collectionId = value.docs.elementAt(0).id;

             print("${collectionId} idddddddddddd");
            name_Ctrl.text=value.docs.elementAt(0)['full_Name'].toString() ;
            birthday_Ctrl.text= value.docs.elementAt(0)['birthday'].toString();
            diseasesl_Ctrl.text = value.docs.elementAt(0)['diseases'].toString();
            governoratel_Ctrl.text =value.docs.elementAt(0)['governorate'].toString();
            _Email_Ctrl.text =value.docs.elementAt(0)['email'].toString() ;
            UserPassWord_Ctrl.text =value.docs.elementAt(0)['password'].toString();
            cases_Ctrl.text =value.docs.elementAt(0)['cases'].toString() ;
            suggest_Ctrl.text =value.docs.elementAt(0)['suggestion'].toString();
            isLoading = true;
          });
        });
      }
  }

  _imageFromCamera() async {
    var image = await ImagePicker().pickImage(source: ImageSource.camera);
    setState(() {});
    if (image != Null) {
      file = File(image!.path);
      var ran = Random().nextInt(10000);
      String imageName = basename(image.name);
      ref = FirebaseStorage.instance.ref("Images/Camera").child(imageName);
      await ref!.putFile(file!);
      imageurl =await ref!.getDownloadURL();
      print(await ref!.getDownloadURL()+"*********************");
    } else {
      return AwesomeDialog(
          context: this.context,
          title: "Important",
          body: Text(
            "please pick image.",
          ),
          dialogType: DialogType.error)
        ..show();
    }
  }
  _imageFromGallery() async {
    var image = await ImagePicker().pickImage(source: ImageSource.gallery);
    setState(() {});
    if (image != Null) {
      file = File(image!.path);
      String imageName =basename(image.name);
      ref = FirebaseStorage.instance.ref("Images/Gallery").child(imageName);
      await  ref!.putFile(file!);
      imageurl =await ref!.getDownloadURL();
      print(await ref!.getDownloadURL()+"*********************");

    } else {
      return AwesomeDialog(
          context: this.context,
          title: "Important",
          body: Text(
            "please pick image.",
          ),
          dialogType: DialogType.error)
        ..show();
    }
  }

  updateInfo()async
  {
    imageurl == null ?Alert.showerror("msg"):null;
    if(widget.loginmethod =="phone")
      {
        CollectionReference   patientreference=FirebaseFirestore.instance.collection("Patients");
        patientreference.doc(collectionId).update({
          "full_Name":name_Ctrl.text,
          "phoneNumber":"${CountryCode}${phone_Ctrl.text}",
          "birthday": birthday_Ctrl.text,
          "governorate":governoratel_Ctrl.text ,
          "secondPhoneNumber":"${CountryCodesecond}${secondPhone_Ctrl.text}",
          "diseases":diseasesl_Ctrl.text,
          "email":"",
          "password":"",
          "cases":cases_Ctrl.text,
          "suggestion":suggest_Ctrl.text,
          "imageUrl":imageurl,
          "signInMethod":"phone",
        });
        Alert.showsucces("تم ارسال المعلومات سيتم التواصل معك بأقرب وقت");
        Get.back();

      }
    else
      {

        CollectionReference   patientreference=FirebaseFirestore.instance.collection("Patients");
        patientreference.doc(collectionId).update({
          "full_Name":name_Ctrl.text,
          "birthday": birthday_Ctrl.text,
          "governorate":governoratel_Ctrl.text ,
          "diseases":diseasesl_Ctrl.text,
          "cases":cases_Ctrl.text,
          "suggestion":suggest_Ctrl.text,
          "imageUrl":imageurl,
          "signInMethod":"phone",
        });
        Alert.showsucces("تم ارسال المعلومات سيتم التواصل معك بأقرب وقت");
        Get.back();
      }
    // Alert.showdialog("...");
    // _firestore.collection("Patients").  add({
    //   "full_Name": "",
    //   "phoneNumber": "${CountryCode}${phone}",
    //   "birthday": "",
    //   "governorate": "",
    //   "secondPhoneNumber": '' ,
    //   "diseases": '',
    //   "email": "",
    //   "password": "",
    //   "cases": '',
    //   "suggestion": '',
    //   "imageUrl" : '',
    //   "signInMethod":"Phone"
    // });
    // Alert.showsucces("تم التسجيل");

  }

  Future<void> getPatient() async {


    if(widget.loginmethod == "phone")
    {
      if ("${widget.phone}" == "${CountryCode}${phone_Ctrl.text}") {
        print("saameeeeeeee***********");
        updateInfo();
      } else {
        print("not saameeeeeeee***********");
        Alert.showdialog("...");
        var collection = FirebaseFirestore.instance.collection('Patients');
        var querySnapshot = await collection
            .where('PhoneNumber', isEqualTo: "${CountryCode}${phone_Ctrl.text}")
            .get();
        if (querySnapshot.docs.length > 0) {
          Alert.showerror("يوجد مستخدم برقم الهاتف هذا");
        } else {
          EasyLoading.dismiss();
          varefynumberFirebase();
        }
      }
    }
    else
      {
        updateInfo();
      }
  }
  FirebaseAuth _auth = FirebaseAuth.instance;
  void varefynumberFirebase() async{
    Alert.showdialog("...");
    print(phone);
    await _auth.verifyPhoneNumber(
      phoneNumber:"${CountryCode}${phone_Ctrl.text} ",
      verificationCompleted: (phoneAuthCredential) async {
        print('fffffffffffffffffffffffff');
        //   EasyLoading.dismiss();
        setState(() {
        });
        //signInWithPhoneAuthCredential(phoneAuthCredential);
      },
      verificationFailed: (verificationFailed) async {
        setState(() {
        });
        EasyLoading.showError( "الرقم غير صالح");
      },
      codeSent: (verificationId, resendingToken) async {
        setState(() {
          print("seeeeeeeeeee");
          EasyLoading.dismiss();
          Get.to(OtpScreen(isLogin: false,varifecationId:verificationId ,phone:"${CountryCode}${phone_Ctrl.text}",register: updateInfo),);
        });
      },
      codeAutoRetrievalTimeout: (verificationId) async {
      },
    );
  }
}




