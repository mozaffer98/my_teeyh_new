import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter/material.dart';
import 'package:flutter_easyloading/flutter_easyloading.dart';
import 'package:get/get.dart';
import 'package:my_teeyh/Helper/alerts/alerts.dart';
import 'package:my_teeyh/screen/Home/doctor_home.dart';
import 'package:my_teeyh/widgets/defualt_btn.dart';
import 'package:my_teeyh/widgets/defualt_text_form_field.dart';
import 'package:shared_preferences/shared_preferences.dart';

class DoctorOtp extends StatefulWidget {
  final bool isLogin;
  final varifecationId,username,phone;
  final Function? register;

   DoctorOtp({Key? key, required this.isLogin, this.varifecationId, this.username, this.phone, required this.register}) : super(key: key);

  @override
  _DoctorOtpState createState() => _DoctorOtpState();
}

class _DoctorOtpState extends State<DoctorOtp> {

  FirebaseAuth _auth = FirebaseAuth.instance;
  TextEditingController _otp_Ctrl=TextEditingController();

  @override
  Widget build(BuildContext context) {
    final size = MediaQuery.of(context).size;

    return Scaffold(
      body: SingleChildScrollView(
          child: Column(
            textDirection: TextDirection.rtl,
            mainAxisAlignment: MainAxisAlignment.end,
            children: [
              SizedBox(
                height: size.height*.07,
              ),
              CircleAvatar(
                radius: 80,
                backgroundColor: Color(0XFF3da7ff),
                child: Image.asset(
                  'images/dent1.jpeg',
                  height: 100,
                  width: 100,
                  fit: BoxFit.cover,
                ),
              ),
              SizedBox(
                height: size.height*.03,
              ),
              Text(
                'تأكيد رمز التحقق',
                style: TextStyle(
                  fontSize: 22,
                  fontWeight: FontWeight.bold,
                ),
              ),
              SizedBox(
                height: size.height*.03,
              ),

              Container(
                margin: EdgeInsets.only(right: size.width*.06,left:size.width*.06 ),
                child: DefaultTextFormField(
                  controller: _otp_Ctrl,
                  type: TextInputType.phone,
                  hint: 'ادخل الكود',
                ),
              ),

              DefaultButton(
                text: "تأكيد",onPress: (){
                if(_otp_Ctrl.text.isEmpty ||_otp_Ctrl.text.length<6||_otp_Ctrl.text.length>6) Alert.showerror("الكود غير صالح");
                else{
                  PhoneAuthCredential phoneAuthCredential =
                  PhoneAuthProvider.credential(
                      verificationId: widget.varifecationId,
                      smsCode: _otp_Ctrl.text
                  );
                  signInWithPhoneAuthCredential(phoneAuthCredential);
                }

              },),
            ],
          )


      ),
    );
  }

  void signInWithPhoneAuthCredential(PhoneAuthCredential phoneAuthCredential) async
  {
    Alert.showdialog("جار التحقق");
    setState(() {
    });

    try {
      final authCredential =
      await _auth.signInWithCredential(phoneAuthCredential);
      setState(() {
      });

      if(authCredential?.user != null){
        EasyLoading.dismiss();
        if(widget.isLogin) {
          SharedPreferences preferences=await  SharedPreferences.getInstance();
          await   preferences.setString("loginmethod","doctorphone" );
          await  preferences.setString("loginmethodandvalue",widget.phone );
          Get.offAll(DoctorHome(loginmethod: "phone", email: "", phone: widget.phone,));
        } else {

          SharedPreferences preferences=await  SharedPreferences.getInstance();
          await   preferences.setString("loginmethod","doctorphone" );
          await  preferences.setString("loginmethodandvalue",widget.phone );
          widget.register!();

        }
        //  Navigator.push(context, MaterialPageRoute(builder: (context)=> Home()));
      }
      else
      {
        EasyLoading.showError("كود خاطئ");
      }

    } on FirebaseAuthException catch (e) {
      EasyLoading.showError("كود خاطئ");

      setState(() {
      });
    }
  }
}
