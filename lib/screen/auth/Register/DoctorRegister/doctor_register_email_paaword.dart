import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:country_code_picker/country_code.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:my_teeyh/Helper/alerts/alerts.dart';
import 'package:my_teeyh/screen/Home/doctor_home.dart';
import 'package:my_teeyh/screen/auth/login/DoctorLogin/doctor_login_home.dart';
import 'package:my_teeyh/screen/auth/login/PatientLogin/patient_login_home.dart';
import 'package:my_teeyh/screen/doctor_page.dart';
import 'package:my_teeyh/widgets/defualt_btn.dart';

class DoctorRegisterEmailPassWord extends StatefulWidget {
  const DoctorRegisterEmailPassWord({Key? key}) : super(key: key);

  @override
  _DoctorRegisterEmailPassWordState createState() => _DoctorRegisterEmailPassWordState();
}

class _DoctorRegisterEmailPassWordState extends State<DoctorRegisterEmailPassWord> {

  TextEditingController _Email_Ctrl=TextEditingController();
  TextEditingController _UserPassWord_Ctrl=TextEditingController();
  TextEditingController _UserPassWordConfirm_Ctrl=TextEditingController();
  TextEditingController name_Ctrl=TextEditingController();

  bool _passwordVisible=false;
  bool _passwordVisible2=false;

  GlobalKey<FormState> formKey = GlobalKey();
  final GlobalKey<ScaffoldState> _scaffoldKey = GlobalKey<ScaffoldState>();

  CollectionReference doctorRef =
  FirebaseFirestore.instance.collection("Doctors");

  addDoctors() async {
    Alert.showdialog("جار التسجيل");
    try{
      var result = await FirebaseAuth.instance.createUserWithEmailAndPassword(
          email: _Email_Ctrl.text.toString().trim(), password: _UserPassWord_Ctrl.text.toString().trim());
      print(result.user!.uid);
      await doctorRef.add({
        "FullName": '${name_Ctrl.text.toString()}',
        "PhoneNumber": "",
        "birthday": '',
        "Univesity": '',
        "Governorate": '',
        "SecondPhoneNumber": "",
        "FinalStage": '',
        "Email": _Email_Ctrl.text.toString().trim(),
        "Password": _UserPassWord_Ctrl.text.toString().trim(),
        "Cases": '',
        "suggestion":  '' ,
        "loginWith":  'email' ,
      });
      Alert.showsucces("تم التسجيل بنجاح");
      Get.offAll(DoctorHome(loginmethod: "email", phone: "",email:_Email_Ctrl.text.toString().trim() ,));
    }
    catch(signUpError){
      Alert.showerror("الايميل مستخدم من قبل");
    }
  }
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      key: _scaffoldKey,
      body: Padding(
        padding: const EdgeInsets.symmetric(horizontal: 8),
        child: Form(
          child: ListView(
            children: [
              SizedBox(
                height: 50,
              ),
              CircleAvatar(
                radius: 80,
                backgroundColor: Color(0XFF3da7ff),
                child: Image.asset(
                  'images/dent1.jpeg',
                  height: 100,
                  width: 100,
                  fit: BoxFit.cover,
                ),
              ),
              SizedBox(
                height: 20,
              ),
              Row(
                mainAxisAlignment: MainAxisAlignment.center,
                children: [
                  const Text(
                    'تسجيل دخول',
                    style: TextStyle(fontSize: 32),
                  ),
                ],
              ),
              TextField(
                controller: _Email_Ctrl,
                onChanged: (value) {},
                decoration: InputDecoration(hintText: 'ادخل البريد الالكتروني',
                  contentPadding: EdgeInsets.symmetric(
                    vertical: 10,
                    horizontal: 20,
                  ),
                  border: OutlineInputBorder(
                    borderRadius: BorderRadius.all(
                      Radius.circular(10),
                    ),
                  ),
                  enabledBorder: OutlineInputBorder(
                    borderSide: BorderSide(
                      width: 1,
                    ),
                    borderRadius: BorderRadius.all(
                      Radius.circular(10),
                    ),
                  ),
                  focusedBorder: OutlineInputBorder(
                    borderSide: BorderSide(
                      color: Colors.blue,
                      width: 2,
                    ),
                    borderRadius: BorderRadius.all(
                      Radius.circular(10),
                    ),
                  ),
                ),
              ),
              SizedBox(height: 8),

              TextFormField(
                obscureText: !_passwordVisible,
                controller: _UserPassWord_Ctrl,
                onChanged: (value) {},
                decoration: InputDecoration(
                  hintText: 'ادخل كلمة المرور',
                  suffixIcon: IconButton(
                  icon: Icon(
                    // Based on passwordVisible state choose the icon
                    _passwordVisible
                        ? Icons.visibility
                        : Icons.visibility_off,
                    color: Theme.of(context).primaryColorDark,
                  ), onPressed: () {
                  setState(() {
                    _passwordVisible = !_passwordVisible;
                  });
                },
                ),

                  contentPadding: EdgeInsets.symmetric(
                    vertical: 10,
                    horizontal: 20,
                  ),
                  border: OutlineInputBorder(
                    borderRadius: BorderRadius.all(
                      Radius.circular(10),
                    ),
                  ),
                  enabledBorder: OutlineInputBorder(
                    borderSide: BorderSide(
                      width: 1,
                    ),
                    borderRadius: BorderRadius.all(
                      Radius.circular(10),
                    ),
                  ),
                  focusedBorder: OutlineInputBorder(
                    borderSide: BorderSide(
                      color: Colors.blue,
                      width: 2,
                    ),
                    borderRadius: BorderRadius.all(
                      Radius.circular(10),
                    ),
                  ),
                ),
              ),
              SizedBox(height: 10),
              TextFormField(
                obscureText:  !_passwordVisible2,
                controller: _UserPassWordConfirm_Ctrl,
                onChanged: (value) {},
                decoration: InputDecoration(
                  hintText: 'تأكيد كلمة المرور',
                  suffixIcon: IconButton(
                    icon: Icon(
                      // Based on passwordVisible state choose the icon
                      _passwordVisible2
                          ? Icons.visibility
                          : Icons.visibility_off,
                      color: Theme.of(context).primaryColorDark,
                    ), onPressed: () {
                    setState(() {
                      _passwordVisible2 = !_passwordVisible2;
                    });
                  },
                  ),

                  contentPadding: EdgeInsets.symmetric(
                    vertical: 10,
                    horizontal: 20,
                  ),
                  border: OutlineInputBorder(
                    borderRadius: BorderRadius.all(
                      Radius.circular(10),
                    ),
                  ),
                  enabledBorder: OutlineInputBorder(
                    borderSide: BorderSide(
                      width: 1,
                    ),
                    borderRadius: BorderRadius.all(
                      Radius.circular(10),
                    ),
                  ),
                  focusedBorder: OutlineInputBorder(
                    borderSide: BorderSide(
                      color: Colors.blue,
                      width: 2,
                    ),
                    borderRadius: BorderRadius.all(
                      Radius.circular(10),
                    ),
                  ),
                ),
              ),
              SizedBox(height: 10),
              TextField(
                controller: name_Ctrl,
                onChanged: (value) {},
                decoration: InputDecoration(hintText: 'الاسم',
                  contentPadding: EdgeInsets.symmetric(
                    vertical: 10,
                    horizontal: 20,
                  ),
                  border: OutlineInputBorder(
                    borderRadius: BorderRadius.all(
                      Radius.circular(10),
                    ),
                  ),
                  enabledBorder: OutlineInputBorder(
                    borderSide: BorderSide(
                      width: 1,
                    ),
                    borderRadius: BorderRadius.all(
                      Radius.circular(10),
                    ),
                  ),
                  focusedBorder: OutlineInputBorder(
                    borderSide: BorderSide(
                      color: Colors.blue,
                      width: 2,
                    ),
                    borderRadius: BorderRadius.all(
                      Radius.circular(10),
                    ),
                  ),
                ),
              ),
              SizedBox(height: 8),


              SizedBox(height: 10),
              DefaultButton(text: "تسجيل", onPress: ()=>vaildation()),

              SizedBox(
                height: 10,
              ),
              // CustomButon(
              //     text: "تسجيل دخول باستخدام الفيس بوك",
              //     onTap: () async {
              //     //  isLoading = true;
              //       setState(() {});
              //     }),
              SizedBox(
                height: 10,
              ),
              Row(
                mainAxisAlignment: MainAxisAlignment.center,
                children: [
                  GestureDetector(
                    onTap: ()=> Get.back(),
                    child:  Text(
                      'دخول',
                      style: TextStyle(color: Color(0XFF3da7ff)),
                    ),
                  ),
                  const Text(
                    ' لديك حساب مسبقا؟ ',
                    style: TextStyle(color: Colors.black),
                  ),
                ],
              ),
            ],
          ),
        ),
      ),
    );
  }

  void vaildation() async {
    if (_Email_Ctrl.text.isEmpty && _UserPassWord_Ctrl.text.isEmpty )
    {
      _scaffoldKey.currentState!.showSnackBar(
        SnackBar(content: Text("جميع الحقول مطلوبة"),),
      );
    } else if (_Email_Ctrl.text.length < 6) {
      _scaffoldKey.currentState!.showSnackBar(
        SnackBar(content: Text("البريد الالكتروني قصير (6 على الاقل)"),),
      );
    } else if (_Email_Ctrl.text.isEmpty) {
      _scaffoldKey.currentState!.showSnackBar(
        SnackBar(content: Text("ادخل البريد"),),
      );
    }  else if (_UserPassWord_Ctrl.text.isEmpty) {
      _scaffoldKey.currentState!.showSnackBar(
        SnackBar(content: Text("ادخل كلمة مرور"),),
      );
    } else if (_UserPassWord_Ctrl.text.length < 8) {
      _scaffoldKey.currentState!.showSnackBar(
        SnackBar(content: Text("كلمة المرور قصيره (8 خانات على الاقل)"),),
      );
    }else if(_UserPassWord_Ctrl.text != _UserPassWordConfirm_Ctrl.text){
      _scaffoldKey.currentState!.showSnackBar(
        SnackBar(content: Text("كلمة المرورغير متطابقة"),),
      );
    } else {
      addDoctors();
    }
  }

}
