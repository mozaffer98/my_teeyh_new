import 'package:flutter/material.dart';
import 'package:my_teeyh/screen/auth/Register/DoctorRegister/doctor_register_email_paaword.dart';
import 'package:my_teeyh/screen/auth/Register/DoctorRegister/doctor_register_phone.dart';

class DoctorRegisterHome extends StatefulWidget {
  const DoctorRegisterHome({Key? key}) : super(key: key);

  @override
  _DoctorRegisterHomeState createState() => _DoctorRegisterHomeState();
}

class _DoctorRegisterHomeState extends State<DoctorRegisterHome> {
  @override
  Widget build(BuildContext context) {
    return DefaultTabController(
      initialIndex: 1,
      length: 2,
      child: Scaffold(
        appBar: AppBar(
          title:  Text(' تسجيل دكتور Doctor Registration'),
          bottom:  TabBar(
            tabs: <Widget>[
              Tab(
                text: "تسجيل حساب بالايميل",
              ),
              Tab(
                text: "تسجيل حساب برقم الهاتف",
              ),
            ],
          ),
        ),
        body:  TabBarView(
          children: <Widget>[
            DoctorRegisterEmailPassWord(),
            DoctorRegisterPhone(),
          ],
        ),
      ),
    );
  }
}
