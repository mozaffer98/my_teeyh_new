import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:country_code_picker/country_code_picker.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter/material.dart';
import 'package:flutter_easyloading/flutter_easyloading.dart';
import 'package:get/get.dart';
import 'package:my_teeyh/Helper/alerts/alerts.dart';
import 'package:my_teeyh/screen/Home/doctor_home.dart';
import 'package:my_teeyh/screen/auth/Otp/doctor_otp.dart';
import 'package:my_teeyh/screen/otp.dart';
import 'package:shared_preferences/shared_preferences.dart';

class DoctorRegisterPhone extends StatefulWidget {
  const DoctorRegisterPhone({Key? key}) : super(key: key);

  @override
  _DoctorRegisterPhoneState createState() => _DoctorRegisterPhoneState();
}

class _DoctorRegisterPhoneState extends State<DoctorRegisterPhone> {
  final _auth=FirebaseAuth.instance;
  String CountryCode='+964', CountryCodesecond='+964';
  TextEditingController phone_ctrl=TextEditingController();
  TextEditingController name_ctrl=TextEditingController();


  GlobalKey<FormState> formKey = GlobalKey();

  CollectionReference doctorRef =
  FirebaseFirestore.instance.collection("Doctors");

  addDoctors() async {
    var formdata = formKey.currentState;
    if (formdata != null) {
      if (formdata.validate()) {
        formdata.save();
        Alert.showdialog("جار التسجيل");
        try{
          await doctorRef.add({
            "FullName": '${name_ctrl.text.toString().trim()}',
            "PhoneNumber": "${CountryCode.toString().trim()}${phone_ctrl.text.toString().trim()}",
            "birthday": '',
            "Univesity": '',
            "Governorate": '',
            "SecondPhoneNumber": "",
            "FinalStage": '',
            "Email": "",
            "Password": "",
            "Cases": '',
            "suggestion":'',
          });
          Alert.showsucces("تم التسجيل بنجاح");
          Get.offAll(DoctorHome(loginmethod: "phone",phone: "${CountryCode.toString().trim()}${phone_ctrl.text.toString().trim()}" ,));
        }
        catch(signUpError){
          Alert.showerror("");
        }


      }
    }
  }


  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Form(
        key: formKey,
        child: ListView(
          children: [
            Padding(
              padding: const EdgeInsets.all(20),
              child: Column(
                children: [
                  Center(
                    child: Text(
                      'تسجيل دكتور Doctor Registration',
                      style: TextStyle(
                        fontSize: 20,
                        fontWeight: FontWeight.bold,
                      ),
                    ),
                  ),
                  Container(
                    height: 45,
                  ),
                  Center(
                    child: CircleAvatar(
                      radius: 90,
                      backgroundColor: Color(0XFF3da7ff),
                      child: CircleAvatar(
                        radius: 80,
                        backgroundImage: AssetImage('images/dent1.jpeg'),
                      ),
                    ),
                  ),
                  Container(
                    height: 45,
                  ),
                  TextFormField(
                    controller: phone_ctrl,
                    validator: (data) {
                      if (data!.isEmpty) {
                        return 'This Question Is Required - هذا السؤال مطلوب';
                      }
                    },
                    keyboardType: TextInputType.phone,
                    decoration: InputDecoration(
                        labelText: 'رقم الهاتف  - First Phone Number',
                        prefixIcon:  CountryCodePicker(
                          onChanged: (c) {
                            CountryCode = c.dialCode!;
                            print(CountryCode);setState(() {});
                          },
                          initialSelection:CountryCode,
                          showCountryOnly: false,
                          showOnlyCountryWhenClosed: false,
                          alignLeft: false,
                        )
                    ),
                  ),
                  Container(
                    height: 45,
                  ),
                  TextFormField(
                    controller: name_ctrl,
                    validator: (data) {
                      if (data!.isEmpty) {
                        return 'This Question Is Required - هذا السؤال مطلوب';
                      }
                    },
                    keyboardType: TextInputType.text,
                    textDirection: TextDirection.rtl,
                    textAlign: TextAlign.start,
                    decoration: InputDecoration(
                        labelText: 'الاسم',
                      hintTextDirection: TextDirection.rtl
                    ),
                  ),
                  Container(
                    height: 45,
                  ),
                  Container(
                    height: 40,
                    width: double.infinity,
                    color: Color(0XFF3da7ff),
                    child: MaterialButton(
                      onPressed: ()  {
                        getDoctor();
                      },
                      child: Text(
                        'تسجيل',
                        style: TextStyle(
                          fontWeight: FontWeight.w900,
                          color: Colors.white,
                        ),
                      ),
                    ),
                  ),
                ],
              ),
            ),
          ],
        ),
      ),
    );
  }
  Future<void> getDoctor() async {
    Alert.showdialog("جار التسجيل");
    var collection = FirebaseFirestore.instance.collection('Doctors');
    var querySnapshot = await collection
        .where('Phone Number', isEqualTo: "${CountryCode.toString().trim()}${phone_ctrl.text.toString().trim()}").get();

    if(querySnapshot.docs.length > 0){
      Alert.showerror(" يوجد مستخدم برقم الهاتف هذا");
      print("noooooooooooooooooooooooo");
    } else {
      print("${CountryCode}${phone_ctrl.text}");
      EasyLoading.dismiss();
      varefynumberFirebase();
      print("yeessssssssss");
    }
  }

  void varefynumberFirebase() async{
    Alert.showdialog("جار التسجيل");
    await _auth.verifyPhoneNumber(
      phoneNumber:"${CountryCode}${phone_ctrl.text.replaceAll(' ', '')}",
      verificationCompleted: (phoneAuthCredential) async {
        print('fffffffffffffffffffffffff');
        //   EasyLoading.dismiss();
        setState(() {
        });
        //signInWithPhoneAuthCredential(phoneAuthCredential);
      },
      verificationFailed: (verificationFailed) async {
        setState(() {
        });
        EasyLoading.showError( "الرقم غير صالح");
      },
      codeSent: (verificationId, resendingToken) async {
        setState(() {
          print("seeeeeeeeeee");
          EasyLoading.dismiss();
          Get.to(DoctorOtp( isLogin: false,varifecationId:verificationId ,phone:"${CountryCode}${phone_ctrl.text.replaceAll(' ', '')}",register: addDoctors),);
        });
      },
      codeAutoRetrievalTimeout: (verificationId) async {
      },
    );
  }


}
