import 'dart:math';
import 'package:awesome_dialog/awesome_dialog.dart';
import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:country_code_picker/country_code_picker.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:firebase_storage/firebase_storage.dart';
import 'package:flutter/material.dart';
import 'package:flutter_easyloading/flutter_easyloading.dart';
import 'package:get/get.dart';
import 'package:image_picker/image_picker.dart';
import 'package:my_teeyh/Helper/alerts/alerts.dart';
import 'package:my_teeyh/screen/otp.dart';
import 'package:my_teeyh/screen/patient_page.dart';
import 'dart:io';
import 'package:path/path.dart';

class LoginPatient extends StatefulWidget {
  const LoginPatient();
  static String id = "Login Patient";
  @override
  State<LoginPatient> createState() => _LoginPatientState();
}

class _LoginPatientState extends State<LoginPatient> {
  GlobalKey<FormState> formKey = GlobalKey();
  String CountryCode='+964', CountryCodesecond='+964';

  File? file;
  String? name;

  String? birthday;

  String? diseases;

  String? governorate;

  String? university;

  String? phone;

  String? secondPhone;

  String? email;

  String? cases;

  String? suggest;
  String? imageurl;
  Reference? ref;
  final _firestore=FirebaseFirestore.instance;


  addPatients() async {
        var formdata = formKey.currentState;
        if (formdata != null) {
          if (formdata.validate()) {
            formdata.save();
            getPatient();
           // registerwithphone();
          }
        }
  }

  void registerwithphone()
  {
    Alert.showdialog("جار التسجيل");
  _firestore.collection("Patients").add({
      "full_Name": "",
      "phoneNumber": "${CountryCode.toString().trim()}${phone!..toString().trim()}",
      "birthday": "",
      "governorate": "",
      "secondPhoneNumber": '' ,
      "diseases": '',
      "email": "",
      "password": "",
      "cases": '',
      "suggestion": '',
      "imageUrl" : '',
      "signInMethod":"Phone"
    });
  Alert.showsucces("تم التسجيل");
  // Get.to();
  }

  _imageFromCamera() async {
    var image = await ImagePicker().pickImage(source: ImageSource.camera);
    setState(() {});
    if (image != Null) {
      file = File(image!.path);
      var ran = Random().nextInt(10000);
      String imageName = "$ran" + basename(image.name);
      ref = FirebaseStorage.instance.ref("Images/Camera").child(imageName);
      await ref!.putFile(file!);
      imageurl =await ref!.getDownloadURL();
      print(await ref!.getDownloadURL()+"*********************");
    } else {
      return AwesomeDialog(
          context: this.context,
          title: "Important",
          body: Text(
            "please pick image.",
          ),
          dialogType: DialogType.error)
        ..show();
    }
  }

  _imageFromGallery() async {
    var image = await ImagePicker().pickImage(source: ImageSource.gallery);
    setState(() {});
    if (image != Null) {
      file = File(image!.path);
      var ran = Random(10000);
      String imageName =basename(image.name);
      ref = FirebaseStorage.instance.ref("Images/Gallery").child(imageName);
      imageurl =await ref!.getDownloadURL().toString();
      print(await ref!.getDownloadURL()+"*********************");
    } else {
      return AwesomeDialog(
          context: this.context,
          title: "Important",
          body: Text(
            "please pick image.",
          ),
          dialogType: DialogType.error)
        ..show();
    }
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Form(
        key: formKey,
        child: ListView(
          children: [
            Padding(
              padding: const EdgeInsets.all(20),
              child: Column(
                children: [
                  Center(
                    child: Text(
                      'تسجيل مريض Patient Registration',
                      style: TextStyle(
                        fontSize: 20,
                        fontWeight: FontWeight.bold,
                      ),
                    ),
                  ),
                  Container(
                    height: 45,
                  ),
                  Center(
                    child: CircleAvatar(
                      radius: 90,
                      backgroundColor: Color(0XFF3da7ff),
                      child: CircleAvatar(
                        radius: 80,
                        backgroundImage: AssetImage('images/dent1.jpeg'),
                      ),
                    ),
                  ),
                  Container(
                    height: 45,
                  ),
                  TextFormField(
                    validator: (data) {
                      if (data!.isEmpty) {
                        return 'This Question Is Required - هذا السؤال مطلوب';
                      }
                    },
                    onSaved: ((value) => phone = value!),
                    keyboardType: TextInputType.phone,
                    decoration: InputDecoration(
                      labelText: 'رقم الهاتف الاول - First Phone Number',
                      prefixIcon:  CountryCodePicker(
                        onChanged: (c) {
                          CountryCode = c.dialCode!;
                          print(CountryCode);setState(() {});
                        },
                        initialSelection:CountryCode,
                        showCountryOnly: false,
                        showOnlyCountryWhenClosed: false,
                        alignLeft: false,
                      )
                    ),
                  ),
                  Container(
                    height: 45,
                  ),
                  Container(
                    height: 40,
                    width: double.infinity,
                    color: Color(0XFF3da7ff),
                    child: MaterialButton(
                      onPressed: ()  {
                         addPatients();
                      },
                      child: Text(
                        'تسجيل',
                        style: TextStyle(
                          fontWeight: FontWeight.w900,
                          color: Colors.white,
                        ),
                      ),
                    ),
                  ),
                ],
              ),
            ),
          ],
        ),
      ),
    );
  }
  FirebaseAuth _auth = FirebaseAuth.instance;

  void varefynumberFirebase() async{
       Alert.showdialog("جار التسجيل");
    print(phone);
    await _auth.verifyPhoneNumber(
      phoneNumber:"${CountryCode}${phone!.replaceAll(' ', '')}",
      verificationCompleted: (phoneAuthCredential) async {
        print('fffffffffffffffffffffffff');
        //   EasyLoading.dismiss();
        setState(() {
        });
        //signInWithPhoneAuthCredential(phoneAuthCredential);
      },
      verificationFailed: (verificationFailed) async {
        setState(() {
        });
         EasyLoading.showError( "${verificationFailed.message}");
      },
      codeSent: (verificationId, resendingToken) async {
        setState(() {
          print("seeeeeeeeeee");
          EasyLoading.dismiss();
          Get.to(OtpScreen(isLogin: false,varifecationId:verificationId ,phone:"${CountryCode}${phone!.replaceAll(' ', '')}",register: registerwithphone),);
        });
      },
      codeAutoRetrievalTimeout: (verificationId) async {
      },
    );
  }
  Future<void> getPatient() async {
    Alert.showdialog("جار التسجيل");
    var collection = FirebaseFirestore.instance.collection('Patients');
    var querySnapshot = await collection
        .where('PhoneNumber', isEqualTo: "${CountryCode.toString().trim()}${{phone!.replaceAll(' ', '')}}").get();
    if(querySnapshot.docs.length > 0){
      Alert.showerror("يوجد مستخدم برقم الهاتف هذا");
      print("noooooooooooooooooooooooo");
    } else {
      EasyLoading.dismiss();
      varefynumberFirebase();
      print("yeessssssssss");
    }
  }
}
