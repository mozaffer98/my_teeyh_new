import 'dart:io';
import 'dart:math';

import 'package:awesome_dialog/awesome_dialog.dart';
import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:firebase_storage/firebase_storage.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:get/get.dart';
import 'package:image_picker/image_picker.dart';
import 'package:my_teeyh/Helper/alerts/alerts.dart';
import 'package:my_teeyh/screen/Home/patientHome.dart';
import 'package:my_teeyh/screen/auth/Register/PatientRegister/patient_register_home.dart';
import 'package:my_teeyh/screen/auth/login/PatientLogin/patient_login_home.dart';
import 'package:path/path.dart';
import 'package:my_teeyh/widgets/custom_button.dart';
import 'package:my_teeyh/widgets/custom_text_home_field.dart';
import 'package:my_teeyh/widgets/defualt_btn.dart';
import 'package:my_teeyh/widgets/defualt_text_form_field.dart';
import 'package:shared_preferences/shared_preferences.dart';


String p =
    r'^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$';

RegExp regExp = new RegExp(p);



class PatientRegisterEmailPassword extends StatefulWidget {
  const PatientRegisterEmailPassword({Key? key}) : super(key: key);

  @override
  _PatientRegisterEmailPasswordState createState() => _PatientRegisterEmailPasswordState();
}

class _PatientRegisterEmailPasswordState extends State<PatientRegisterEmailPassword> {
  TextEditingController _Email_Ctrl=TextEditingController();
  TextEditingController _UserPassWord_Ctrl=TextEditingController();
  TextEditingController _UserPassWordConfirm_Ctrl=TextEditingController();
  final GlobalKey<ScaffoldState> _scaffoldKey = GlobalKey<ScaffoldState>();

  GlobalKey<FormState> formKey = GlobalKey();
  final _firestore=FirebaseFirestore.instance;
  Reference? ref;

  bool _passwordVisible=false;
  bool _passwordVisible2=false;

  void vaildation() async {
    if (_Email_Ctrl.text.isEmpty && _UserPassWord_Ctrl.text.isEmpty )
    {
      _scaffoldKey.currentState!.showSnackBar(
        SnackBar(content: Text("جميع الحقول مطلوبة"),),
      );
    } else if (_Email_Ctrl.text.length < 6) {
      _scaffoldKey.currentState!.showSnackBar(
        SnackBar(content: Text("البريد الالكتروني قصير (6 على الاقل)"),),
      );
    } else if (_Email_Ctrl.text.isEmpty) {
      _scaffoldKey.currentState!.showSnackBar(
        SnackBar(content: Text("ادخل البريد"),),
      );
    }  else if (_UserPassWord_Ctrl.text.isEmpty) {
      _scaffoldKey.currentState!.showSnackBar(
        SnackBar(content: Text("ادخل كلمة مرور"),),
      );
    } else if (_UserPassWord_Ctrl.text.length < 8) {
      _scaffoldKey.currentState!.showSnackBar(
        SnackBar(content: Text("كلمة المرور قصيره (8 خانات على الاقل)"),),
      );
    }else if(_UserPassWord_Ctrl.text != _UserPassWordConfirm_Ctrl.text){
      _scaffoldKey.currentState!.showSnackBar(
        SnackBar(content: Text("كلمة المرورغير متطابقة"),),
      );
    } else {
      submit();
    }
  }





  @override
  Widget build(BuildContext context) {
    return Scaffold(
      key: _scaffoldKey,
      body: Padding(
        padding: const EdgeInsets.symmetric(horizontal: 8),
        child: Form(
          child: ListView(
            children: [
              SizedBox(
                height: 50,
              ),
              CircleAvatar(
                radius: 80,
                backgroundColor: Color(0XFF3da7ff),
                child: Image.asset(
                  'images/dent1.jpeg',
                  height: 100,
                  width: 100,
                  fit: BoxFit.cover,
                ),
              ),
              SizedBox(
                height: 20,
              ),
              Row(
                mainAxisAlignment: MainAxisAlignment.center,
                children: [
                  const Text(
                    'تسجيل دخول',
                    style: TextStyle(fontSize: 32),
                  ),
                ],
              ),

              TextField(
                controller: _Email_Ctrl,
                onChanged: (value) {},
                decoration: InputDecoration(hintText: 'ادخل البريد الالكتروني',
                  contentPadding: EdgeInsets.symmetric(
                    vertical: 10,
                    horizontal: 20,
                  ),
                  border: OutlineInputBorder(
                    borderRadius: BorderRadius.all(
                      Radius.circular(10),
                    ),
                  ),
                  enabledBorder: OutlineInputBorder(
                    borderSide: BorderSide(
                      width: 1,
                    ),
                    borderRadius: BorderRadius.all(
                      Radius.circular(10),
                    ),
                  ),
                  focusedBorder: OutlineInputBorder(
                    borderSide: BorderSide(
                      color: Colors.blue,
                      width: 2,
                    ),
                    borderRadius: BorderRadius.all(
                      Radius.circular(10),
                    ),
                  ),
                ),
              ),
              SizedBox(height: 8),

              TextField(
                obscureText: !_passwordVisible,
                controller: _UserPassWord_Ctrl,
                onChanged: (value) {},
                decoration: InputDecoration(
                  hintText: 'ادخل كلمة المرور',
                  suffixIcon: IconButton(
                    icon: Icon(
                      // Based on passwordVisible state choose the icon
                      _passwordVisible
                          ? Icons.visibility
                          : Icons.visibility_off,
                      color: Theme.of(context).primaryColorDark,
                    ), onPressed: () {
                    setState(() {
                      _passwordVisible = !_passwordVisible;
                    });
                  },
                  ),
                  contentPadding: EdgeInsets.symmetric(
                    vertical: 10,
                    horizontal: 20,
                  ),
                  border: OutlineInputBorder(
                    borderRadius: BorderRadius.all(
                      Radius.circular(10),
                    ),
                  ),
                  enabledBorder: OutlineInputBorder(
                    borderSide: BorderSide(
                      width: 1,
                    ),
                    borderRadius: BorderRadius.all(
                      Radius.circular(10),
                    ),
                  ),
                  focusedBorder: OutlineInputBorder(
                    borderSide: BorderSide(
                      color: Colors.blue,
                      width: 2,
                    ),
                    borderRadius: BorderRadius.all(
                      Radius.circular(10),
                    ),
                  ),
                ),
              ),
              SizedBox(height: 10),
              TextField(
                obscureText:  !_passwordVisible2,
                controller: _UserPassWordConfirm_Ctrl,
                onChanged: (value) {},
                decoration: InputDecoration(
                  hintText: 'تأكيد كلمة المرور',
                  suffixIcon: IconButton(
                    icon: Icon(
                      // Based on passwordVisible state choose the icon
                      _passwordVisible2
                          ? Icons.visibility
                          : Icons.visibility_off,
                      color: Theme.of(context).primaryColorDark,
                    ), onPressed: () {
                    setState(() {
                      _passwordVisible2 = !_passwordVisible2;
                    });
                  },
                  ),
                  contentPadding: EdgeInsets.symmetric(
                    vertical: 10,
                    horizontal: 20,
                  ),
                  border: OutlineInputBorder(
                    borderRadius: BorderRadius.all(
                      Radius.circular(10),
                    ),
                  ),
                  enabledBorder: OutlineInputBorder(
                    borderSide: BorderSide(
                      width: 1,
                    ),
                    borderRadius: BorderRadius.all(
                      Radius.circular(10),
                    ),
                  ),
                  focusedBorder: OutlineInputBorder(
                    borderSide: BorderSide(
                      color: Colors.blue,
                      width: 2,
                    ),
                    borderRadius: BorderRadius.all(
                      Radius.circular(10),
                    ),
                  ),
                ),
              ),



              SizedBox(height: 10),
              DefaultButton(text: "دخول", onPress: ()=>vaildation()),

              SizedBox(
                height: 10,
              ),
              // CustomButon(
              //     text: "تسجيل دخول باستخدام الفيس بوك",
              //     onTap: () async {
              //     //  isLoading = true;
              //       setState(() {});
              //     }),
              SizedBox(
                height: 10,
              ),
              Row(
                mainAxisAlignment: MainAxisAlignment.center,
                children: [
                  GestureDetector(
                    onTap: ()=> Get.offAll(PatientLoginHome()),
                    child:  Text(
                      'دخول',
                      style: TextStyle(color: Color(0XFF3da7ff)),
                    ),
                  ),
                  const Text(
                    ' لديك حساب مسبقا؟ ',
                    style: TextStyle(color: Colors.black),
                  ),
                ],
              ),
            ],
          ),
        ),
      ),
    );
  }
  void submit() async
  {

    Alert.showdialog("جار التسجيل");
    try {
      setState(() {});
      UserCredential  result =
      await FirebaseAuth.instance.createUserWithEmailAndPassword(
          email: _Email_Ctrl.text.toString().trim(),
          password: _UserPassWord_Ctrl.text.toString().trim());
      _firestore.collection("Patients").add({
        "full_Name": '',
        "phoneNumber": "",
        "birthday": '',
        "governorate": '',
        "secondPhoneNumber": '' ,
        "diseases": '',
        "email":_Email_Ctrl.text ,
        "password": _UserPassWord_Ctrl.text,
        "cases": '',
        "suggestion":  '' ,
        "imageUrl": '',
        "signInMethod":"email"
      });

      SharedPreferences preferences=await  SharedPreferences.getInstance();
      await   preferences.setString("loginmethod","patientemail" );
      await  preferences.setString("loginmethodandvalue",_Email_Ctrl.text.toString().trim() );

      Alert.showsucces("تم التسجيل بنجاح");
      Get.offAll(PAtientHome(loginmethod: "email",email:_Email_Ctrl.text.toString().trim() ,));
     // Navigator.of(context).pushAndRemoveUntil(MaterialPageRoute(builder: (BuilderVontext)=>HomeScreen(email: result.user.email,)), (route) => false);
    }
    on PlatformException catch (error) {
      Alert.showerror("conne");
      var message = "Please Check Your Internet Connection ";
      if (error.message != null) {
        message = error.message!;
      }
      setState(() {
      });
    }
    catch (error) {
      Alert.showerror("الايميل مستخدم من قبل");
      setState(() {
      });

      print(error);
    }

    // Navigator.of(context)
    //     .pushReplacement(MaterialPageRoute(builder: (ctx) => Login()));
    setState(() {
    });


  }

}
