import 'package:flutter/material.dart';
import 'package:my_teeyh/screen/auth/Register/PatientRegister/patient_register_email_password.dart';
import 'package:my_teeyh/screen/auth/Register/PatientRegister/patient_register_phone.dart';
import 'package:my_teeyh/screen/auth/Register/PatientRegister/login_patient.dart';

class PatientRegisterHome extends StatefulWidget {
  const PatientRegisterHome({Key? key}) : super(key: key);

  @override
  _PatientRegisterHomeState createState() => _PatientRegisterHomeState();
}

class _PatientRegisterHomeState extends State<PatientRegisterHome> {
  @override
  Widget build(BuildContext context) {
    return DefaultTabController(
      initialIndex: 1,
      length: 2,
      child: Scaffold(
        appBar: AppBar(
          title:  Text('تسجيل حساب مريض'),
          centerTitle: true,
     //     backgroundColor:Color(0xFF1C2D57),
          bottom:  TabBar(
            tabs: <Widget>[
              Tab(
                text: "تسجيل بالايميل",
              ),
              Tab(
                text: "تسجيل برقم الهاتف",
              ),
            ],
          ),
        ),
        body:  TabBarView(
          children: <Widget>[
            PatientRegisterEmailPassword(),
            LoginPatient(),
          ],
        ),
      ),
    );
  }
}
