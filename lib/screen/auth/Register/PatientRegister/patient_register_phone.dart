import 'package:flutter/material.dart';
import 'package:my_teeyh/widgets/custom_button.dart';
import 'package:my_teeyh/widgets/custom_text_home_field.dart';
import 'package:my_teeyh/widgets/defualt_btn.dart';
import 'package:my_teeyh/widgets/defualt_text_form_field.dart';

class PatientRegisterPhone extends StatefulWidget {
  const PatientRegisterPhone({Key? key}) : super(key: key);

  @override
  _PatientRegisterPhoneState createState() => _PatientRegisterPhoneState();
}

class _PatientRegisterPhoneState extends State<PatientRegisterPhone> {

  TextEditingController phone_ctrl=TextEditingController();

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Padding(
        padding: const EdgeInsets.symmetric(horizontal: 8),
        child: Form(
          child: ListView(
            children: [
              SizedBox(
                height: 50,
              ),
              CircleAvatar(
                radius: 80,
                backgroundColor: Color(0XFF3da7ff),
                child: Image.asset(
                  'images/dent1.jpeg',
                  height: 100,
                  width: 100,
                  fit: BoxFit.cover,
                ),
              ),
              SizedBox(
                height: 20,
              ),
              Row(
                mainAxisAlignment: MainAxisAlignment.center,
                children: [
                  const Text(
                    'تسجيل حساب',
                    style: TextStyle(fontSize: 32),
                  ),
                ],
              ),
              SizedBox(
                height: 20,
              ),

              DefaultTextFormField(
                  hint: "ادخل رقم الهاتف",
                  controller: phone_ctrl,
                type: TextInputType.phone,

              ),
              SizedBox(
                height: 10,
              ),

             DefaultButton(text: "تسجيل",
                 onPress:(){

                 } ),
              SizedBox(
                height: 10,
              ),
              // CustomButon(
              //     text: "تسجيل دخول باستخدام الفيس بوك",
              //     onTap: () async {
              //       //  isLoading = true;
              //       setState(() {});
              //     }),
              SizedBox(
                height: 10,
              ),
            ],
          ),
        ),
      ),
    );
  }


}
