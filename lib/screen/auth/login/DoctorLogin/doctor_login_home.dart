import 'package:flutter/material.dart';
import 'package:my_teeyh/screen/auth/login/DoctorLogin/doctor_login_email_password.dart';
import 'package:my_teeyh/screen/auth/login/DoctorLogin/doctor_login_phone.dart';

class DoctorLoginHome extends StatefulWidget {
  const DoctorLoginHome({Key? key}) : super(key: key);

  @override
  _DoctorLoginHomeState createState() => _DoctorLoginHomeState();
}

class _DoctorLoginHomeState extends State<DoctorLoginHome> {
  @override
  Widget build(BuildContext context) {
    return DefaultTabController(
      initialIndex: 1,
      length: 2,
      child: Scaffold(
        appBar: AppBar(
          centerTitle: true,
          title:  Text('دخول دكتور'),
       //   backgroundColor:Color(0xFF1C2D57),
          bottom:  TabBar(
            tabs: <Widget>[
              Tab(
                text: "تسجيل دخول بالايميل",
              ),
              Tab(
                text: "تسجيل دخول برقم الهاتف",
              ),
            ],
          ),
        ),
        body:  TabBarView(
          children: <Widget>[
            DoctorLoginEmailPassWord(),
            DoctorLoginPhone(),
          ],
        ),
      ),
    );
  }
}
