import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:get/get.dart';
import 'package:my_teeyh/Helper/alerts/alerts.dart';
import 'package:my_teeyh/screen/Home/patientHome.dart';
import 'package:my_teeyh/screen/auth/Register/PatientRegister/patient_register_home.dart';
import 'package:my_teeyh/widgets/custom_button.dart';
import 'package:my_teeyh/widgets/custom_text_home_field.dart';
import 'package:my_teeyh/widgets/defualt_btn.dart';
import 'package:shared_preferences/shared_preferences.dart';

import '../../verify_email.dart';

class PatientLoginEmailPassWord extends StatefulWidget {
  const PatientLoginEmailPassWord({Key? key}) : super(key: key);

  @override
  _PatientLoginEmailPassWordState createState() => _PatientLoginEmailPassWordState();
}

class _PatientLoginEmailPassWordState extends State<PatientLoginEmailPassWord> {
  GlobalKey<FormState> keyForm = GlobalKey();
  TextEditingController _Email_Ctrl=TextEditingController();
  TextEditingController _UserPassWord_Ctrl=TextEditingController();
  final _auth=FirebaseAuth.instance;
  final GlobalKey<ScaffoldState> _scaffoldKey = GlobalKey<ScaffoldState>();
  bool _passwordVisible=false;

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      key: _scaffoldKey,
      body: Padding(
        padding: const EdgeInsets.symmetric(horizontal: 8),
        child: Form(
          key: keyForm,
          child: ListView(
            children: [
              SizedBox(
                height: 50,
              ),
              CircleAvatar(
                radius: 80,
                backgroundColor: Color(0XFF3da7ff),
                child: Image.asset(
                  'images/dent1.jpeg',
                  height: 100,
                  width: 100,
                  fit: BoxFit.cover,
                ),
              ),
              SizedBox(
                height: 20,
              ),
              Row(
                mainAxisAlignment: MainAxisAlignment.center,
                children: [
                  const Text(
                    'تسجيل دخول',
                    style: TextStyle(fontSize: 32),
                  ),
                ],
              ),

          TextField(
            controller: _Email_Ctrl,
            onChanged: (value) {},
            decoration: InputDecoration(hintText: 'ادخل البريد الالكتروني',
              contentPadding: EdgeInsets.symmetric(
                vertical: 10,
                horizontal: 20,
              ),
              border: OutlineInputBorder(
                borderRadius: BorderRadius.all(
                  Radius.circular(10),
                ),
              ),
              enabledBorder: OutlineInputBorder(
                borderSide: BorderSide(
                  width: 1,
                ),
                borderRadius: BorderRadius.all(
                  Radius.circular(10),
                ),
              ),
              focusedBorder: OutlineInputBorder(
                borderSide: BorderSide(
                  color: Colors.blue,
                  width: 2,
                ),
                borderRadius: BorderRadius.all(
                  Radius.circular(10),
                ),
              ),
            ),
          ),
          SizedBox(height: 8),

          TextFormField(
            controller: _UserPassWord_Ctrl,
            onChanged: (value) {},
            obscureText:   !_passwordVisible,
            decoration: InputDecoration(
              hintText: 'ادخل كلمة المرور',
              suffixIcon: IconButton(
                icon: Icon(
                  // Based on passwordVisible state choose the icon
                  _passwordVisible
                      ? Icons.visibility
                      : Icons.visibility_off,
                  color: Theme.of(context).primaryColorDark,
                ), onPressed: () {
                setState(() {
                  _passwordVisible = !_passwordVisible;
                });
              },
              ),
              contentPadding: EdgeInsets.symmetric(
                vertical: 10,
                horizontal: 20,
              ),
              border: OutlineInputBorder(
                borderRadius: BorderRadius.all(
                  Radius.circular(10),
                ),
              ),
              enabledBorder: OutlineInputBorder(
                borderSide: BorderSide(
                  width: 1,
                ),
                borderRadius: BorderRadius.all(
                  Radius.circular(10),
                ),
              ),
              focusedBorder: OutlineInputBorder(
                borderSide: BorderSide(
                  color: Colors.blue,
                  width: 2,
                ),
                borderRadius: BorderRadius.all(
                  Radius.circular(10),
                ),
              ),
            ),
          ),



          SizedBox(height: 10),
            DefaultButton(text: "دخول", onPress: ()=>vaildation()),

            SizedBox(
                height: 10,
              ),
              // CustomButon(
              //     text: "تسجيل دخول باستخدام الفيس بوك",
              //     onTap: () async {
              //     //  isLoading = true;
              //       setState(() {});
              //     }),
              SizedBox(
                height: 10,
              ),
              Row(
                mainAxisAlignment: MainAxisAlignment.center,
                children: [
                  GestureDetector(
                  onTap: ()=> Get.to(ResetScreen()),
                    child:  Text(
                      'نسيت كلمة المرور؟',
                      style: TextStyle(color: Color(0XFF3da7ff)),
                    ),
                  ),

                ],
              ),
              SizedBox(
                height: 10,
              ),

              Row(
                mainAxisAlignment: MainAxisAlignment.center,
                children: [
                  GestureDetector(
                    onTap: ()=> Get.to(PatientRegisterHome()),
                    child:  Text(
                      'انشاء حساب',
                      style: TextStyle(color: Color(0XFF3da7ff)),
                    ),
                  ),
                  const Text(
                    'ليس لديك حساب مسبقا؟ ',
                    style: TextStyle(color: Colors.black),
                  ),
                ],
              ),
            ],
          ),
        ),
      ),
    );
  }

  void vaildation() async {
    if (_Email_Ctrl.text.isEmpty && _UserPassWord_Ctrl.text.isEmpty )
    {
      _scaffoldKey.currentState!.showSnackBar(
        SnackBar(content: Text("جميع الحقول مطلوبة"),),
      );
    } else if (_Email_Ctrl.text.length < 6) {
      _scaffoldKey.currentState!.showSnackBar(
        SnackBar(content: Text("البريد الالكتروني قصير (6 على الاقل)"),),
      );
    } else if (_Email_Ctrl.text.isEmpty) {
      _scaffoldKey.currentState!.showSnackBar(
        SnackBar(content: Text("ادخل البريد"),),
      );
    }  else if (_UserPassWord_Ctrl.text.isEmpty) {
      _scaffoldKey.currentState!.showSnackBar(
        SnackBar(content: Text("ادخل كلمة مرور"),),
      );
    } else if (_UserPassWord_Ctrl.text.length < 8) {
      _scaffoldKey.currentState!.showSnackBar(
        SnackBar(content: Text("كلمة المرور قصيره (8 خانات على الاقل)"),),
      );
    } else {
     submit();
    }
  }
  void submit() async
  {
    Alert.showdialog("جار تسجيل الدخول");
    try {
      setState(() {});
      final user = await FirebaseAuth.instance.signInWithEmailAndPassword(
          email: _Email_Ctrl.text.toString().trim(),
          password: _UserPassWord_Ctrl.text.toString().trim());
      if (user != null) {
        print(user.user!.uid);
        SharedPreferences preferences=await  SharedPreferences.getInstance();
        await   preferences.setString("loginmethod","patientemail" );
        await  preferences.setString("loginmethodandvalue",_Email_Ctrl.text.toString().trim() );

        Alert.showsucces("تم تسجيل الدخول بنجاح");
        Get.offAll(PAtientHome(loginmethod: "email",email:_Email_Ctrl.text.toString().trim() ,));
      }
    }
    on PlatformException catch (error) {
      Alert.showerror("خطأ في البريد او كلمة المرور");
      var message = "Please Check Your Internet Connection ";
      if (error.message != null) {
        message = error.message!;
      }
      (() {});
    }
    catch (error) {
      Alert.showerror("خطأ في البريد او كلمة المرور");
      setState(() {});
      _scaffoldKey.currentState!.showSnackBar(SnackBar(
        content: Text("خطأ في البريد او كلمة المرور"),
        duration: Duration(seconds: 6),
        backgroundColor: Theme
            .of(context)
            .primaryColor,
      ));

      print(error);
    }

    // Navigator.of(context)
    //     .pushReplacement(MaterialPageRoute(builder: (ctx) => Login()));
    setState(() {});
  }
  }
