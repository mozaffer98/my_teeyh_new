import 'package:flutter/material.dart';
import 'package:my_teeyh/screen/auth/login/PatientLogin/patient_login_email_password.dart';
import 'package:my_teeyh/screen/auth/login/PatientLogin/patient_login_phone.dart';

class PatientLoginHome extends StatefulWidget {
  const PatientLoginHome({Key? key}) : super(key: key);

  @override
  _PatientLoginHomeState createState() => _PatientLoginHomeState();
}

class _PatientLoginHomeState extends State<PatientLoginHome> {
  @override
  Widget build(BuildContext context) {
    return DefaultTabController(
      initialIndex: 1,
      length: 2,
      child: Scaffold(
        appBar: AppBar(
          centerTitle: true,
          title:  Text('دخول مريض'),
        //  backgroundColor:Color(0xFF1C2D57),
          bottom:  TabBar(
            tabs: <Widget>[
              Tab(
                text: "تسجيل دخول بالايميل",
              ),
              Tab(
                text: "تسجيل دخول برقم الهاتف",
              ),
            ],
          ),
        ),
        body:  TabBarView(
          children: <Widget>[
            PatientLoginEmailPassWord(),
            PatientLoginPhone(),
          ],
        ),
      ),
    );
  }
}
