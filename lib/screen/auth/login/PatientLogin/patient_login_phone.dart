import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:country_code_picker/country_code_picker.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter/material.dart';
import 'package:flutter_easyloading/flutter_easyloading.dart';
import 'package:get/get.dart';
import 'package:my_teeyh/Helper/alerts/alerts.dart';
import 'package:my_teeyh/screen/auth/Register/PatientRegister/patient_register_home.dart';
import 'package:my_teeyh/widgets/defualt_btn.dart';
import 'package:my_teeyh/widgets/defualt_text_form_field.dart';

import '../../../otp.dart';

class PatientLoginPhone extends StatefulWidget {
  const PatientLoginPhone({Key? key}) : super(key: key);

  @override
  _PatientLoginPhoneState createState() => _PatientLoginPhoneState();
}

class _PatientLoginPhoneState extends State<PatientLoginPhone> {

  TextEditingController phone_ctrl=TextEditingController();
  String CountryCode='+964';
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Form(
        child: ListView(
          children: [
            Padding(
              padding: const EdgeInsets.all(20),
              child: Column(
                children: [
                  Center(
                    child: Text(
                      'تسجيل دخول مريض ',
                      style: TextStyle(
                        fontSize: 20,
                        fontWeight: FontWeight.bold,
                      ),
                    ),
                  ),
                  Container(
                    height: 45,
                  ),
                  Center(
                    child: CircleAvatar(
                      radius: 90,
                      backgroundColor: Color(0XFF3da7ff),
                      child: CircleAvatar(
                        radius: 80,
                        backgroundImage: AssetImage('images/dent1.jpeg'),
                      ),
                    ),
                  ),

                  SizedBox(height: 40,),

                  DefaultTextFormField(
                    hint: 'ادخل لرقم الهاتف',
                    controller: phone_ctrl,
                    prefixIcon:  CountryCodePicker(
                      onChanged: (c) {
                        CountryCode = c.dialCode!;
                        print(CountryCode);setState(() {});
                      },
                      initialSelection:CountryCode,
                      showCountryOnly: false,
                      showOnlyCountryWhenClosed: false,
                      alignLeft: false,
                    ),
                  ),
                  DefaultButton(
                    text: 'دخول',
                    onPress: (){
                      getPatient();

                    },
                  ),
                  SizedBox(
                    height: 10,
                  ),
                  Row(
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: [
                      GestureDetector(
                        onTap: ()=> Get.to(PatientRegisterHome()),
                        child:  Text(
                          'انشاء حساب',
                          style: TextStyle(color: Color(0XFF3da7ff)),
                        ),
                      ),
                       Text(
                        'ليس لديك حساب مسبقا؟ ',
                        style: TextStyle(color: Colors.black),
                      ),
                    ],
                  ),


                  // Container(
                  //   margin: EdgeInsets.all(8),
                  //   child: Row(
                  //     mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  //     children: [
                  //       TextButton(
                  //           onPressed: () {
                  //             Get.to(const SignUpScreen());
                  //           },
                  //           child: Text(
                  //             'sign_in_as_client'.tr,
                  //           )),
                  //       GestureDetector(
                  //         onTap: ()=>buildLanguageDialog(context),
                  //         child: Container(
                  //           child: Row(
                  //             children: [
                  //               Text("Choose_your_language".tr),
                  //               Icon(Icons.language,),
                  //
                  //             ],
                  //           ),
                  //         ),
                  //       ),
                  //
                  //     ],
                  //   ),
                  // ),

                ],
              ),
            ),
          ],
        ),
      ),
    );
  }
  final _auth=FirebaseAuth.instance;
  final _firestore=FirebaseFirestore.instance;

 Future<void> getPatient() async {
   print("${CountryCode}${phone_ctrl.text.replaceAll(' ', '')}");
   Alert.showdialog("جار تسجيل الدخول");
   var collection = FirebaseFirestore.instance.collection('Patients');
   var querySnapshot = await collection
       .where('phoneNumber', isEqualTo: "${CountryCode}${phone_ctrl.text.replaceAll(' ', '')}").get();
   if(querySnapshot.docs.length > 0){
     EasyLoading.dismiss();
     varefynumberFirebase();
     print("yeessssssssss");
   } else {
    Alert.showerror("لا يوجد مستخدم برقم الهاتف هذا");
     print("noooooooooooooooooooooooo");
   }
  }
  void varefynumberFirebase() async{
    print("${CountryCode}${phone_ctrl.text.replaceAll(' ', '')}");
   Alert.showdialog("جار تسجيل الدخول");
    await _auth.verifyPhoneNumber(
      phoneNumber:"${CountryCode}${phone_ctrl.text.replaceAll(' ', '')}",
      verificationCompleted: (phoneAuthCredential) async {
        print('fffffffffffffffffffffffff');
        //   EasyLoading.dismiss();
        setState(() {
        });
        //signInWithPhoneAuthCredential(phoneAuthCredential);
      },
      verificationFailed: (verificationFailed) async {
        setState(() {
        });
        EasyLoading.showError( "${verificationFailed.message}");
      },
      codeSent: (verificationId, resendingToken) async {
        setState(() {
          print("seeeeeeeeeee");
          EasyLoading.dismiss();
        Get.to(OtpScreen(isLogin: true,varifecationId:verificationId ,phone:"${CountryCode}${phone_ctrl.text.replaceAll(' ', '')}",register: (){}),);
        });
      },
      codeAutoRetrievalTimeout: (verificationId) async {
      },
    );
  }


}
