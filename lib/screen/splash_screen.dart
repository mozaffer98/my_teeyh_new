import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:my_teeyh/screen/Home/doctor_home.dart';
import 'package:my_teeyh/screen/Home/patientHome.dart';
import 'package:my_teeyh/screen/home_page.dart';
import 'package:shared_preferences/shared_preferences.dart';


class SplashScreen extends StatefulWidget {
  const SplashScreen({Key? key}) : super(key: key);

  @override
  State<SplashScreen> createState() => _SplashScreenState();
}

class _SplashScreenState extends State<SplashScreen> {
  @override
  void initState() {
    super.initState();
  }

   @override
  Widget build(BuildContext context) {
     Future.delayed(
       Duration(seconds: 4),
      ()async {
        SharedPreferences prefs = await SharedPreferences.getInstance();
        switch (prefs.get("loginmethod")) {
          case null:
          Get.offAll(HomePage());
            break;
          case "patientphone":
            Get.offAll(PAtientHome(phone:prefs.get("loginmethodandvalue") ,loginmethod:"phone" ,));
            break;
          case "patientemail":
            Get.offAll(PAtientHome(phone:prefs.get("loginmethodandvalue") ,loginmethod:"email" ,));
            break;
          case "doctorphone":
            Get.offAll(DoctorHome(phone:prefs.get("loginmethodandvalue") ,loginmethod:"phone" ,));            break;
          case "doctoremail":
            Get.offAll(DoctorHome(phone:prefs.get("loginmethodandvalue") ,loginmethod:"email" ,));            break;
        }
      },
    );
    return Scaffold(
      body: SafeArea(
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          children:  [
            Center(
              child:CircleAvatar(
                radius: 80,
                backgroundColor: Color(0XFF3da7ff),
                child: Image.asset(
                  'images/dent1.jpeg',
                  height: 100,
                  width: 100,
                  fit: BoxFit.cover,
                ),
              ),
            ),
          ],
        ),
      ),
    );
  }
}
