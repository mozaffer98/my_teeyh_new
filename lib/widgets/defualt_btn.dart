import 'package:flutter/material.dart';
import 'package:get/get.dart';

class DefaultButton extends StatelessWidget {
  final double? width;
  final String text;
  final VoidCallback onPress;

  const DefaultButton({
    Key? key,
    this.width,
    required this.text,
    required this.onPress,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    final size = MediaQuery.of(context).size;

    return Padding(
      padding: EdgeInsets.symmetric(
        horizontal: width ?? size.width * .06,
        vertical: size.height *.035
      ),
      child: Container(
        width: double.infinity,
        decoration: BoxDecoration(
          borderRadius: BorderRadius.circular(8.0),
          color: Get.theme.primaryColor,
        ),
        child: MaterialButton(
          onPressed: onPress,
          child: Text(
            text,
            style: const TextStyle(
              color: Colors.white,
              fontSize: 14,
              fontFamily: 'Tajawal',
            ),
          ),
        )

      ),
    );
  }
}
