import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

class DefaultTextFormField extends StatelessWidget {
  final TextEditingController controller;

  final String hint;
  final  validate;
  final Widget? prefixIcon;
  final Widget? prefix;
  final double? horizontalPadding;
  final double? verticalPadding;
  final TextInputType? type;

  const DefaultTextFormField({
    Key? key,
    required this.hint,
    this.validate,
    required this.controller,
    this.prefixIcon,
    this.horizontalPadding,
    this.verticalPadding,
    this.type, this.prefix,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    final size = MediaQuery.of(context).size;

    return Padding(
      padding: EdgeInsets.symmetric(
        horizontal: horizontalPadding ?? size.width * .06,
        vertical: verticalPadding ?? size.height * .005,
      ),
      child: Container(
        decoration: BoxDecoration(
          color: Colors.white,
          borderRadius: BorderRadius.circular(8),
        ),
        height: size.height * .06,
        child: TextFormField(
          textAlign: TextAlign.right,
          textAlignVertical: TextAlignVertical.bottom,
          controller: controller,
          validator: validate,
          keyboardType: type ?? TextInputType.phone,
          decoration: InputDecoration(
            border: const OutlineInputBorder(),
            hintText: hint,
            hintStyle: const TextStyle(
              fontFamily: 'Tajawal',
            ),
            suffixIcon: prefixIcon,
            prefix: prefix,
          ),
        ),
      ),
    );
  }
}
class DefaultTextFormField1 extends StatelessWidget {
  final TextEditingController controller;

  final String hint;
  final String? Function(String?)? validate;
  final Widget? prefixIcon;
  final Widget? prefix;
  final double? horizontalPadding;
  final double? verticalPadding;
  final TextInputType? type;

  const DefaultTextFormField1({
    Key? key,
    required this.hint,
    this.validate,
    required this.controller,
    this.prefixIcon,
    this.horizontalPadding,
    this.verticalPadding,
    this.type, this.prefix,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    final size = MediaQuery.of(context).size;

    return Padding(
      padding: EdgeInsets.symmetric(
        horizontal: horizontalPadding ?? size.width * .06,
        vertical: verticalPadding ?? size.height * .035,
      ),
      child: Container(
        decoration: BoxDecoration(
          color: Colors.white,
          borderRadius: BorderRadius.circular(8),
        ),
        height: size.height * .19,
        child: TextFormField(
          maxLines: 10,
          textAlignVertical: TextAlignVertical.bottom,
          controller: controller,
          textDirection: TextDirection.rtl,
          validator: validate,
          keyboardType: type ?? TextInputType.phone,
          decoration: InputDecoration(
            border: const OutlineInputBorder(),
            hintText: hint,
            hintTextDirection: TextDirection.rtl,
            hintStyle: const TextStyle(
              fontFamily: 'Tajawal',
            ),
            prefixIcon: prefixIcon,
            prefix: prefix,
          ),
        ),
      ),
    );
  }
}
