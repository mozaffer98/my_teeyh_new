import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:shared_preferences/shared_preferences.dart';

import '../screen/splash_screen.dart';



class MainDrawer extends StatefulWidget {

  final   VoidCallback onPressprofile,onpresslogout;
  MainDrawer({required this.onPressprofile, required this.onpresslogout});

  @override
  State<MainDrawer> createState() => _MainDrawerState();
}

class _MainDrawerState extends State<MainDrawer> {
  int radioGroup = 2;
  String? name,phone;
  Future<Null> getprofiledata() async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    name = prefs.getString("name");
    phone = prefs.getString("phone");
    setState(() {
    });
  }




  @override
  void initState() {
    // TODO: implement initState
    super.initState();
  //  getprofiledata();
  }

  @override
  Widget build(BuildContext context) {
    return Drawer(
      child: ListView(
        padding: EdgeInsets.zero,
        children: <Widget>[
          DrawerHeader(
            decoration: BoxDecoration(),
            child: Center(
              child: CircleAvatar(
                radius: 90,
                backgroundColor: Color(0XFF3da7ff),
                child: CircleAvatar(
                  radius: 80,
                  backgroundImage: AssetImage('images/dent1.jpeg'),
                ),
              ),
            )
          ),
          ListTile(
            //    onTap: () => Get.to(RepetedQuestion()),
            trailing : Text('القائمة'),
          ),
          ListTile(
           onTap: () => Get.back(),
           title: Text('الرئيسية'),
            trailing: const Icon(
              Icons.home,
              color: Colors.grey,
            ),
          ),
          // ListTile(
          //   onTap:widget.addoffer,
          //   title: Text('إضافة عرض'),
          //   trailing: const Icon(
          //     Icons.add ,
          //     color: Colors.grey,
          //   ),
          // ),
          ListTile(
            onTap:widget.onPressprofile,
            title: Text('الملف الشخصي'),
            trailing: const Icon(
              Icons.person,
              color: Colors.grey,
            ),
          ),

          Container(
            margin: EdgeInsets.only(top: 2),
            child: ListTile(
              onTap: widget.onpresslogout,
              // leading: SvgPicture.asset(AppImages.travelsImage),
              title: Text('تسجيل الخروج',style: TextStyle(fontSize: 16)),
              trailing: const Icon(
                Icons.logout,
              ),

            ),
          ),
        ],
      ),
    );
  }

}



class MainDrawerDoctor extends StatefulWidget {
  final   VoidCallback onPressprofile,onpresslogout,onpressaddoffer;

   MainDrawerDoctor({Key? key, required this.onPressprofile, required this.onpresslogout, required this.onpressaddoffer}) : super(key: key);

  @override
  _MainDrawerDoctorState createState() => _MainDrawerDoctorState();
}

class _MainDrawerDoctorState extends State<MainDrawerDoctor> {
  @override
  Widget build(BuildContext context) {
    return Drawer(
      child: ListView(
        padding: EdgeInsets.zero,
        children: <Widget>[
          DrawerHeader(
              decoration: BoxDecoration(),
              child: Center(
                child: CircleAvatar(
                  radius: 90,
                  backgroundColor: Color(0XFF3da7ff),
                  child: CircleAvatar(
                    radius: 80,
                    backgroundImage: AssetImage('images/dent1.jpeg'),
                  ),
                ),
              )
          ),
          ListTile(
            //    onTap: () => Get.to(RepetedQuestion()),
            trailing : Text('القائمة'),
          ),
          ListTile(
            onTap: () => Get.back(),
            title: Text('الرئيسية'),
            trailing: const Icon(
              Icons.home,
              color: Colors.grey,
            ),
          ),
          ListTile(
            onTap: widget.onpressaddoffer ,
            title: Text('إضافة عرض'),
            trailing: const Icon(
              Icons.add_box_outlined,
              color: Colors.grey,
            ),
          ),
          // ListTile(
          //   onTap:widget.addoffer,
          //   title: Text('إضافة عرض'),
          //   trailing: const Icon(
          //     Icons.add ,
          //     color: Colors.grey,
          //   ),
          // ),
          ListTile(
            onTap:widget.onPressprofile,
            title: Text('الملف الشخصي'),
            trailing: const Icon(
              Icons.person,
              color: Colors.grey,
            ),
          ),

          Container(
            margin: EdgeInsets.only(top: 2),
            child: ListTile(
              onTap: widget.onpresslogout,
              // leading: SvgPicture.asset(AppImages.travelsImage),
              title: Text('تسجيل الخروج',style: TextStyle(fontSize: 16)),
              trailing: const Icon(
                Icons.logout,
              ),

            ),
          ),
        ],
      ),
    );
  }
}