import 'package:flutter/material.dart';
import 'package:get/get.dart';

class ProfileCard extends StatelessWidget {

  final String? user_name,phone;

  const ProfileCard({Key? key, this.user_name, this.phone}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    final size = MediaQuery.of(context).size;
    return Container(
      margin: const EdgeInsets.symmetric(vertical: 30),
      height:size.width*.30 ,
      child: ListTile(
        leading: const CircleAvatar(
          radius: 20,
          child: Icon(Icons.person),
        ),
        title:  Text("${user_name}",
          style: TextStyle(
              color: Colors.white, fontSize: 16, fontWeight: FontWeight.bold),
        ),
        subtitle: Row(
          children: [
            Icon(Icons.phone,color: Colors.white,),
            SizedBox(width: 5,),
            Text(
              '${phone}',
              overflow: TextOverflow.ellipsis,
              maxLines: 1,
              style: TextStyle(
                color: Colors.white,
              ),
            ),
          ],
        ),
      ),
    );
  }

}